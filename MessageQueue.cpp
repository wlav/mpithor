#include "mpithor/config.h"
#define MPITHOR_INTERNAL 1
#include "mpithor/debug.h"
#include "mpithor/MessageQueue.h"
#include "mpithor/Reordering.h"

#include <iostream>
#include <assert.h>


//
// Queue message request: each type of message sits in its own queue.
//
void* mpithor::MessageQueue::enqueue_request(void *buf, int count, MPI_Datatype datatype,
        int target, int tag, ptrdiff_t comm, void *request, void *errflag)
{
    MPITHOR_TRACER(2);

    uint64_t slot = (m_head+1) % REQ_QUEUE_LENGTH;
    mem_req_t& cur_request = m_requests[slot];

    if (cur_request.state && cur_request.state != r_completed) {
    // slot belongs to an outstanding message ... need bigger queue (could force
    // completion, but that requires keeping track of requests, which would be,
    // relatively, much slower).
        std::cerr << "No slot available and failed to drain queue: "
                  << cur_request.state << std::endl;
        exit(3);
    }

// take over cur_request
    m_head++;

    cur_request.buf      = buf;
    cur_request.count    = count;
    cur_request.datatype = datatype;
    cur_request.target   = target;
    cur_request.tag      = tag;
    cur_request.comm     = comm;
    cur_request.request  = request;
    cur_request.errflag  = errflag;

    cur_request.state  = r_pending;

// Only add request into buffer, issue all the requests in ends of comm regions or
// when entering MPI_Wait/MPI_Waitall
    return (void*)&cur_request;
}

#ifdef MPICH
void* mpithor::MessageQueue::enqueue_request(
    const void *sendbuf, MPI_Aint sendcount, MPI_Datatype sendtype, int dest, int sendtag,
    void *recvbuf, MPI_Aint recvcount, MPI_Datatype recvtype, int source, int recvtag,
    void *comm_ptr, MPI_Status *status, void *errflag)
{
    MPITHOR_TRACER(2);

    mem_req_t& cur_request = *(mem_req_t*)enqueue_request(
        (void*)sendbuf, sendcount, sendtype, dest, sendtag, (ptrdiff_t)comm_ptr, status, errflag);

    cur_request.tbuf      = recvbuf;
    cur_request.tcount    = recvcount;
    cur_request.tsource   = source;
    cur_request.tdatatype = recvtype;
    cur_request.ttag      = recvtag;

    return (void*)&cur_request;
}
#endif

