#include "mpithor/config.h"

#define MPITHOR_INTERNAL 1
#include "mpithor/debug.h"
#include "mpithor/internal.h"
#include "mpithor/smpi.h"
#include "mpithor/datatypes.h"
#include "mpithor/MessageTimings.h"

#include <chrono>
#include <iostream>
#include <map>
#include <set>
#include <vector>

#include <assert.h>
#include <math.h>

#define NUM_STARTUP_EXPERIMENTS 5
#define NUM_TIMINGS_EXPERIMENTS 100
#define BLOCK_SIZE 16384


// Range of measured message sizes; this assumes (based on experimental data) that
// small is roughly the same all-over, and that 512B - 16K is more feature rich.
// Size is limited to 128K, b/c of measurement cost.
const size_t MESSAGE_SIZES[] = {
   32, 64, 128,                        // includes 64B protocol switch-over on Cray
   512, 768, 1024, 1536, 2048, 3072,   // feature-rich block (on Cray)
   4096, 6144, 8192, 16384,            // includes gasnet protocol switch-over on Cray
   32768, 65536, 131072 };             // representative enough of all large sizes
const size_t N_MSG_SIZES = sizeof(MESSAGE_SIZES)/sizeof(size_t);

// mapping from round(log(blocksize)*3)-10.0 to lower bound (except
// for edge); then linearly extrapolate
// Message sizes:
//  [ 0.0,  2.0,  5.0,  9.0, 10.0, 11.0, 12.0, 13.0,
//   14.0, 15.0, 16.0, 17.0, 19.0, 21.0, 23.0, 25.0]

const size_t INDEX_MAPPING[] = {
     0,  0,  1,  1,  1,
     2,  2,  2,  2,  3,
     4,  5,  6,  7,  8,
     9, 10, 12, 12, 13,
    13, 14, 14, 15, 15
};
const size_t N_IDX_MAPPINGS = sizeof(INDEX_MAPPING)/sizeof(size_t);

inline size_t M_INDEX(size_t blocksize) {
    assert(MESSAGE_SIZES[0] <= blocksize);
    assert(blocksize <= MESSAGE_SIZES[N_MSG_SIZES-1]);
    size_t index = (size_t)round(log(blocksize)*3)-10;
    assert(0 <= index && index <= N_IDX_MAPPINGS);
    return INDEX_MAPPING[index];
}

namespace mpithor {

typedef std::chrono::steady_clock clock_t;

static inline double time_spent(
        const clock_t::time_point& start, const clock_t::time_point& end) {
    return std::chrono::duration_cast<std::chrono::microseconds>(end - start).count();
}

} // namespace mpithor

static mpithor::latency_t g_send_injection[N_MSG_SIZES];
static mpithor::latency_t g_send_latencies[N_MSG_SIZES];


//
// collect all relevant latency timing data; only called of a reordering
// is selected that will make use of it
//
int mpithor::collect_timings(int concurrency) {
// Select a rank to be "representative": pick the sending ranks over the
// whole available spectrum, then communicate with each other rank and
// pick the one that is slowest on average. The idea is that with the
// spread out choice of sending ranks, this receiving rank will be a
// mixture of communication links away from them.
    if (g_world_size <= concurrency) {
        if (!g_world_rank)
            std::cerr << "[mpithor] too few ranks for measuring with concurrency "
                      << concurrency << std::endl;
        exit(5);
    }

// collect the senders
    double factor = concurrency/(double)g_world_size;
    std::set<int> senders;
    bool amSender = false;
    for (int irank = 0; irank < g_world_size; ++irank) {
        if (round(irank*factor) != round((irank+1)*factor)) {
            senders.insert(irank);
            if (irank == g_world_rank) amSender = true;
        }
    }
    assert((int)senders.size() == concurrency);

// allocated buffers for receivers
    std::vector<std::vector<char>> buffers;
    std::vector<MPI_Request> requests;
    if (amSender) {
    // need just one buffer
        buffers.resize(1);
        buffers[0].resize(BLOCK_SIZE);
    } else {
    // need a buffer for each sender
        buffers.resize(concurrency);
        for (int i = 0; i < concurrency; ++i)
            buffers[i].resize(BLOCK_SIZE);
        requests.resize(concurrency);
    }

    latency_t* results = new latency_t[g_world_size];

// time communications
    for (int recv_rank = 0; recv_rank < g_world_size; ++recv_rank) {
    // if the recv_rank is a sender, assume that this will not be the
    // slowest of the bunch and simply skip
        if (senders.find(recv_rank) != senders.end()) {
            results[recv_rank] = 0.;
            continue;
        }

    // run experiments
        latency_t tot_time = 0.;
        MPI_Barrier(MPI_COMM_WORLD);
        for (int iexp = 0; iexp < NUM_STARTUP_EXPERIMENTS; ++iexp) {

            if (recv_rank == g_world_rank) {
            // our turn ... post receives for all senders
                int isnd = 0;
                for (auto send_rank : senders) {
                    MPI_Irecv(&buffers[isnd][0], BLOCK_SIZE, MPI_CHAR,
                              send_rank, 0, MPI_COMM_WORLD, &requests[isnd]);
                    isnd += 1;
                }
                MPI_Waitall(senders.size(), &requests[0], MPI_STATUS_IGNORE);
            }

            if (amSender) {
                auto comm_start = clock_t::now();  
                MPI_Request request;
                MPI_Isend(&buffers[0][0], BLOCK_SIZE, MPI_CHAR,
                          recv_rank, 0, MPI_COMM_WORLD, &request);
                MPI_Wait(&request, MPI_STATUS_IGNORE);
                tot_time += time_spent(comm_start, clock_t::now());
            }

            MPI_Barrier(MPI_COMM_WORLD);
        }

        results[recv_rank] = tot_time;
    }

// decide on which recv_rank
    latency_t* allsums = new latency_t[g_world_size];
    MPI_Allreduce(results, allsums, g_world_size*sizeof(latency_t), MPI_CHAR, MPI_SUM, MPI_COMM_WORLD);

    int test_rank = -1;
    double slowest = 0.;
    for (int irank = 0; irank < g_world_size; ++irank) {
        if (slowest < allsums[irank]) {
            slowest = allsums[irank];
            test_rank = irank;
        }
    }

    assert(0 <= test_rank);

    delete [] allsums;
    delete [] results;

// collect injection and latency times over the range of block sizes
    for (size_t isz = 0; isz < N_MSG_SIZES; ++isz) {
        MPI_Barrier(MPI_COMM_WORLD);
        for (int iexp = 0; iexp < NUM_TIMINGS_EXPERIMENTS; ++iexp) {

            if (test_rank == g_world_rank) {
            // post receives for all senders
                int isnd = 0;
                for (auto send_rank : senders) {
                    MPI_Irecv(&buffers[isnd][0], BLOCK_SIZE, MPI_CHAR,
                              send_rank, 0, MPI_COMM_WORLD, &requests[isnd]);
                    isnd += 1;
                }
                MPI_Waitall(senders.size(), &requests[0], MPI_STATUS_IGNORE);
            }

            if (amSender) {
                auto comm_start = clock_t::now();  
                MPI_Request request;
                MPI_Isend(&buffers[0][0], BLOCK_SIZE, MPI_CHAR,
                          test_rank, 0, MPI_COMM_WORLD, &request);
                auto inj_end = clock_t::now();
                MPI_Wait(&request, MPI_STATUS_IGNORE);
                g_send_latencies[isz] += time_spent(inj_end, clock_t::now());
                g_send_injection[isz] += time_spent(comm_start, inj_end);
            }

            MPI_Barrier(MPI_COMM_WORLD);
        }
    }

    return MPI_SUCCESS;
}


namespace mpithor {

// helper to capture common indexing for injection and latency timings
static inline latency_t interpolate(latency_t* data, size_t blocksize) {
    if (blocksize <= MESSAGE_SIZES[0])
        return data[0];

    if (blocksize >= MESSAGE_SIZES[N_MSG_SIZES-1])
        return data[N_MSG_SIZES-1];

    size_t m_index = M_INDEX(blocksize);
    assert(m_index < N_MSG_SIZES-1);
    double low  = data[m_index];
    double high = data[m_index+1];
    double scale = MESSAGE_SIZES[m_index+1]-MESSAGE_SIZES[m_index];
    return low + (blocksize-MESSAGE_SIZES[m_index])/scale*(high-low);
}

} // namespace mpithor

//
// Get actual measured injection timings of sending a specific block size
// message to a "typical" target.
//
mpithor::latency_t mpithor::send_injection(size_t blocksize) {
    return interpolate(g_send_injection, blocksize);
}

//
// Get actual measured latencies of sending a specific block size message
// to a "typical" target.
//
mpithor::latency_t mpithor::send_latency(size_t blocksize) {
    return interpolate(g_send_latencies, blocksize);
}
