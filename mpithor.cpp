#include "mpithor/config.h"

#define MPITHOR_INTERNAL 1
#include "mpithor/debug.h"
#include "mpithor/internal.h"
#include "mpithor/smpi.h"
#include "mpithor/mpithor.h"
#include "mpithor/callstats.h"
#include "mpithor/MessageQueue.h"
#include "mpithor/Reordering.h"

int MPITHOR_VERBOSE      =  0;
int MPITHOR_VERBOSE_RANK = -1;

bool mpithor::MPITHOR_REORDER_RECVS = true;
bool mpithor::MPITHOR_ANALYSIS_IN_BARRIER = false;
bool mpithor::MPITHOR_ANALYSIS_IN_ALLREDUCE = false;
bool mpithor::MPITHOR_KEYED_ANALYSIS = true;

int mpithor::MPITHOR_ANALYSIS_RANK = 2;

int mpithor::g_world_rank = 0;
int mpithor::g_world_size = 0;

extern mpithor::MessageQueue g_msg_queue[];

//
int mpithor_initialize()
{
    MPITHOR_TRACER(1);

// whether we want debugging output or not
    const char* coption = getenv("MPITHOR_VERBOSE");
    if (coption) MPITHOR_VERBOSE = atoi(coption);

    coption = getenv("MPITHOR_VERBOSE_RANK");
    if (coption) MPITHOR_VERBOSE_RANK = atoi(coption);

// whether to reverse order recv's from sends
    coption = getenv("MPITHOR_REORDER_RECVS");
    if (coption) mpithor::MPITHOR_REORDER_RECVS = bool(atoi(coption));

// whether to analyze inside barriers or on regular interval (TODO: auto-detect)
    coption = getenv("MPITHOR_ANALYSIS_IN_BARRIER");
    if (coption) mpithor::MPITHOR_ANALYSIS_IN_BARRIER = bool(atoi(coption));

// whether to analyze inside allreduces or on regular interval (TODO: auto-detect)
    coption = getenv("MPITHOR_ANALYSIS_IN_ALLREDUCE");
    if (coption) mpithor::MPITHOR_ANALYSIS_IN_ALLREDUCE = bool(atoi(coption));

// rank to use for analysis
    coption = getenv("MPITHOR_ANALYSIS_RANK");
    if (coption) mpithor::MPITHOR_ANALYSIS_RANK = atoi(coption);

// save our world rank and size for convenience
    MPI_Comm_rank(MPI_COMM_WORLD, &mpithor::g_world_rank);
    MPI_Comm_size(MPI_COMM_WORLD, &mpithor::g_world_size);

    if (!mpithor::g_world_rank) {
        if (MPITHOR_VERBOSE) {
            std::cerr << "[mpithor] Verbosity level:    " << MPITHOR_VERBOSE << std::endl;
            if (0 <= MPITHOR_VERBOSE_RANK)
                std::cerr << "[mpithor] Verbosity rank:     " << MPITHOR_VERBOSE_RANK << std::endl;
        }
    }

// reorder-schedule specific information (also creates reordering itself, allowing
// this to be done prior to client code)
    return mpithor::Reordering::get()->initialize();
}


//
// allow app-level temporary disabling of reordering
//
static mpithor::EState old_state = mpithor::EO_DISABLED;
void mpithor_start_pause()
{
    MPITHOR_TRACER(1);
    old_state = mpithor::g_state;
    mpithor::g_state = mpithor::EO_DISABLED;
}

void mpithor_end_pause()
{
    MPITHOR_TRACER(1);
    mpithor::g_state = old_state;
}


//
// start a communication region where we are to reorder
//
void mpithor_start_region(int /* msgtype */, int marker)
{
// if we get here while pending, then that means there is no sync outside of
// the communication region, which is a performance bug
    MPITHOR_TRACER(1);
    assert(marker != DEFAULT_REGION_MARKER); // default is only for internal use
    assert(marker != MPIC_INTERNAL_MARKER);  // id
    mpithor::g_state = mpithor::EO_ANALYSIS_ON;
#ifndef NDEBUG
    assert(mpithor::g_current_region == DEFAULT_REGION_MARKER);
    mpithor::g_current_region = marker;
#endif
    mpithor::Reordering::get()->new_region(marker);
}

//
// finish a communication region where we are to reorder
//
void mpithor_end_region(int msgtype, int marker)
{
// finish up issueing all outstanding requests, but the communication completion
// will come later when the client application calls it
    MPITHOR_TRACER(1);
    assert(mpithor::g_state == mpithor::EO_ANALYSIS_ON); // ie. seen start
    assert(marker != DEFAULT_REGION_MARKER); // default is only for internal use
    assert(marker != MPIC_INTERNAL_MARKER);  // id
    mpithor::Reordering::get()->issue_request_bundled(g_msg_queue[msgtype], mpithor::all);
    mpithor::g_state = mpithor::EO_ANALYSIS_PENDING;
    mpithor::Reordering::get()->new_region(DEFAULT_REGION_MARKER);
#ifndef NDEBUG
    assert(marker == mpithor::g_current_region);
    mpithor::g_current_region = DEFAULT_REGION_MARKER;
#endif
}

//
// allow a (reorder-specific) "system reset"
//
int mpithor_reset()
{
    MPITHOR_TRACER(1);
    if (MPITHOR_VERBOSE && !mpithor::g_world_rank)
        std::cerr << "[mpithor:0] resetting reordering\n";
    return mpithor::Reordering::get()->reset();
}

//
// cleanup, print stats etc.
//
int mpithor_finalize()
{
    mpithor::g_callstats.print_stats(mpithor::MPITHOR_ANALYSIS_RANK);
    return 0;
}

//
// run the analysis, after enough data is collected, immediately after waitsync
//
void mpithor_set_immediate_analysis()
{
    using namespace mpithor;
    MPITHOR_TRACER(1);
    MPITHOR_ANALYSIS_IN_BARRIER = MPITHOR_ANALYSIS_IN_ALLREDUCE = false;
}

//
// defer the analysis, after enough data is collected, into the next barrier
//
void mpithor_set_deferred_analysis()
{
    using namespace mpithor;
    MPITHOR_TRACER(1);
    MPITHOR_ANALYSIS_IN_BARRIER = MPITHOR_ANALYSIS_IN_ALLREDUCE = true;
}

//
// switch on/off analysis on (size, neighbor) pairs
//
void mpithor_set_keyed_analysis()
{
    MPITHOR_TRACER(1);
    mpithor::MPITHOR_KEYED_ANALYSIS = true;
}

void mpithor_set_no_keyed_analysis()
{
    MPITHOR_TRACER(1);
    mpithor::MPITHOR_KEYED_ANALYSIS = false;
}
