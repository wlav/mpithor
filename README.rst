.. -*- mode: rst -*-

MPIThor: Message Reordering for MPI
===================================

Usage: replace includes of mpi.h with MPIThor's smpi.h.
Recompile and relink with libmpithor.so.
If needed, the API to drive MPIThor and to identify communication regions is
made available by including mpithor.h.

API calls are:

 * *mpithor_initialize()*

   Normally called by sMPI_Initialize(), but can be called explicitly if
   MPI_Initialize is not covered through including smpi.h.

 * *mpithor_start_region(int msgtype, int marker)*

   Delineates the start of a communication region.
   msgtype is one of MPITHOR_ISEND_MSG, MPITHOR_IRECV_MSG, MPITHOR_ISENDC_MSG
   or MPITHOR_IRECVC_MSG.
   The marker has to match Irecv's with Isends.

 * *mpithor_end_region(int msgtype, int marker)*

   Similar to start; this delineates the end of a communication region.

 * *mpithor_finalize()*

   Cleanup MPIThor, print stats, etc.

Relevant environment variables are:

 * *MPITHOR_REORDERING_SCHEME*

   The strategy used for reordering.
   One of: immediate (no reordering); highlow (sort by latency, high to low);
   lowhigh (sort by latency, low to high); interleave (odd ranks run highlow,
   even ranks run lowhigh); local (predictive estimator for optimal pair
   ordering); and global (measurement and heuristic driven global schedule).

 * *MPITHOR_REORDER_RECVS*

   Order Irecvs in the same order as Isends. (Default is 1 (true).)

 * *MPITHOR_GLOBAL_DECISION_INTERVAL*

   For global schedule only: the number of iterations measured, before running
   the analysis and reordering. (Default is 100.)

 * *MPITHOR_GLOBAL_DECISION_WARMUP*

   For global schedule only: the number of iterations to ignore before
   starting measurements. (Default is 5.)

 * *MPITHOR_GLOBAL_DECISION_REMEASURE*

   Number of iterations before full restart of measurements and analysis.
   (Default is 1000.)

 * *MPITHOR_ANALYSIS_IN_BARRIER*

   For global schedule only: hide the analysis in a barrier following the
   communication region.

 * *MPITHOR_ANALYSIS_IN_ALLREDUCE*

   For global schedule only: perform the analysis in allreduces (no hiding).

 * *MPITHOR_KEYED_ANALYSIS*

   For global schedule only: run an analysis per bundle, with keys defined
   by message sizes send and the ranks communicated with.

 * *MPITHOR_ANALYSIS_RANK*

   For global schedule only: the rank that will run the analysis.
   (Default is 2.)

 * *MPITHOR_VERBOSE*

   Print verbose information (level 1-5).
   Requires NDEBUG to be undefined.
   Off by default.

 * *MPITHOR_VERBOSE_RANK*

   Restrict verbose output to just this rank.
