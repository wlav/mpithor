WRAPPER_LDFLAGS:=
ifeq ($(LINK_OVERRIDES),yes)
   INTERNAL_WRAPPER_LDFLAGS:= -Wl,--wrap=MPI_Init -Wl,--wrap=MPI_Init_thread -Wl,--wrap=MPI_Finalize -Wl,--wrap=MPI_Isend -Wl,--wrap=MPI_Irecv -Wl,--wrap=MPI_Wait -Wl,--wrap=MPI_Waitall -Wl,--wrap=MPI_Barrier -Wl,--wrap=MPI_Allreduce -Wl,--wrap=MPI_Alltoall -Wl,--wrap=MPIC_Isend -Wl,--wrap=MPIC_Irecv -Wl,--wrap=MPIC_Sendrecv -Wl,--wrap=MPIC_Waitall
   WRAPPER_LDFLAGS+= -static $(INTERNAL_WRAPPER_LDFLAGS)
   CPPFLAGS+= -DMPITHOR_LINK_OVERRIDES
   CFLAGS+= -DMPITHOR_LINK_OVERRIDES
endif
