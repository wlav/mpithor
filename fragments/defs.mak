ifeq ($(NERSC_HOST),cori)
   CC:=cc
   CXX:=CC
   MPICC:=cc
   MPICXX:=CC
   EXTRA_FLAGS += -DCORI_=1
else ifeq ($(NERSC_HOST),edison)
   CC:=cc
   CXX:=CC
   MPICC:=cc
   MPICXX:=CC
   EXTRA_FLAGS += -DEDISON_=1
else ifeq ($(NERSC_HOST), shepard)
   CXX:=icpc
   CC:=icc
   MPICC:=mpicc
   MPICXX:=mpicxx
   EXTRA_FLAGS += -DSHEPARD_=1 -pthread
else ifeq ($(NERSC_HOST), cerebro)
   CC:=gcc
   CXX:=g++
   MPICC:=mpicc
   MPICXX:=mpicxx
   EXTRA_FLAGS += -DCEREBRO_=1
endif

CPPFLAGS=-I$(IDIR) -std=c++11 -fPIC -Wall
CFLAGS=-I$(IDIR) -Wall

ifeq ($(OPT),yes)
   CFLAGS+= -O2 -fPIC -DNDEBUG -DMPITHOR_NO_TRACING
   CPPFLAGS+= -O2 -fPIC -DNDEBUG -DMPITHOR_NO_TRACING
endif
