#define MPITHOR_INTERNAL 1
#include "mpithor/debug.h"
#include "mpithor/smpi.h"
#include "mpithor/callstats.h"

#include <iostream>


mpithor::CallStats mpithor::g_callstats;

void mpithor::CallStats::print_stats(int node)
{
    if (node == GET_MYRANK()) {
        std::cerr << "mpithor stats for rank:  " << node
                  << "\n  RECV:                    " << num_recv
                  << "\n  RECVC:                   " << num_recvc
                  << "\n  SEND:                    " << num_send
                  << "\n  SENDC:                   " << num_sendc
                  << "\n  SENDRECVC:               " << num_sendrecvc
                  << "\n  WAIT:                    " << num_wait
                  << "\n  WAITC:                   " << num_waitc
                  << "\n  WAITALL:                 " << num_waitall
                  << "\n  WAITALLC:                " << num_waitallc
                  << "\n  BARRIER:                 " << num_barrier
                  << "\n    in-barrier analyses:   " << num_bar_analyses
                  << "\n  ALLREDUCE:               " << num_allreduce
                  << "\n    in-allreduce analyses: " << num_allred_analyses
                  << "\n  # of bundles:            " << num_bundle_issued
                  << "\n     of average size:      " << \
                        (num_bundle_issued ? tot_bundle_size/num_bundle_issued : 0)
                  << "\n  # analyses run:          " << num_analyses;
              if (num_analyses) {
                  std::cerr
                  << "\n      time spent (us):     " << analysis_time / num_analyses
                  << "\n      # global:            " << num_global
                  << "\n      # local:             " << num_local
                  << "\n      # won:               " << num_won
                  << "\n      # lost:              " << num_lost;
              }
              std::cerr << std::endl;
        MPI_Barrier(MPI_COMM_WORLD);
    } else {
        MPI_Barrier(MPI_COMM_WORLD);
    }
}
