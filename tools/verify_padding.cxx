#include <mpi.h>
#define MPITHOR_PADDING_VERIFY 1
#include "../mpithor/datatypes.h"

#include <iostream>

int main() {
    std::cerr << "size of record: " << sizeof(mpithor::mem_req_t) << std::endl;
    std::cerr << "padding needed: " << 64 - (sizeof(mpithor::mem_req_t) % 64) << std::endl;
    return 0;
}
