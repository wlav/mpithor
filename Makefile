all: libmpithor.so libmpithor.a

.PHONY: clean all

IDIR=mpithor

include fragments/defs.mak
include fragments/wrappers.mak

IDIR=mpithor

#add .h files here
DEPS := $(wildcard $(IDIR)/*.h) Makefile $(wildcard fragments/*.mak)

# targets
MPITHOR_SRCS=$(wildcard *.cpp)
MPITHOR_OBJS=$(MPITHOR_SRCS:cpp=o)

%.o: %.cpp $(DEPS)
	$(MPICXX) -c -o $@ $< $(CPPFLAGS) $(EXTRA_FLAGS)

libmpithor.so: $(DEPS) $(MPITHOR_OBJS)
	$(MPICXX) -shared -o libmpithor.so $(CPPFLAGS) $(MPITHOR_OBJS) $(INTERNAL_WRAPPER_LDFLAGS)

libmpithor.a: $(DEPS) $(MPITHOR_OBJS)
	ar ru libmpithor.a  $(MPITHOR_OBJS)

clean:
	rm -f $(MPITHOR_OBJS) libmpithor.*
