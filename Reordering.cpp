#include "mpithor/config.h"

#define MPITHOR_INTERNAL 1
#include "mpithor/debug.h"
#include "mpithor/internal.h"
#include "mpithor/smpi.h"
#include "mpithor/mpithor.h"
#include "mpithor/datatypes.h"
#include "mpithor/callstats.h"
#include "mpithor/MessageTimings.h"
#include "mpithor/MessageQueue.h"
#include "mpithor/Reordering.h"

#include <algorithm>
#include <chrono>
#include <iomanip>
#include <iostream>
#include <numeric>
#include <vector>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>

typedef uint16_t order_t;

#define BUNDLE_SIZE 8


//
// currently available reorderings:
//   - "immediate"  : just issue (no reorder)
//   - "highlow"    : sort by latency, then issue ordered high to low
//   - "lowhigh"    : sort by latency, then issue ordered low to high
//   - "interleave" : mynode%2 high-low, !mynode%2 low-high
//   - "global"     : use statistical measures to agree on a global schedule
//
std::unique_ptr<mpithor::Reordering> mpithor::Reordering::get_reordering()
{
    std::unique_ptr<Reordering> reordering = nullptr;

    int rank = GET_MYRANK();
    const char* cscheme = getenv("MPITHOR_REORDERING_SCHEME");
    if (!cscheme || !strcmp(cscheme, "immediate")) {
        cscheme = "immediate";
        reordering = std::unique_ptr<Reordering>(new Immediate{});
    } else if (!strcmp(cscheme, "highlow")) {
        reordering = std::unique_ptr<Reordering>(new HighLow{BUNDLE_SIZE});
    } else if (!strcmp(cscheme, "lowhigh")) {
        reordering = std::unique_ptr<Reordering>(new LowHigh{BUNDLE_SIZE});
    } else if (!strcmp(cscheme, "interleave")) {
        if (GET_MYRANK()%2)
            reordering = std::unique_ptr<Reordering>(new HighLow{BUNDLE_SIZE});
        else
            reordering = std::unique_ptr<Reordering>(new LowHigh{BUNDLE_SIZE});
    } else if (!strcmp(cscheme, "local")) {
        reordering = std::unique_ptr<Reordering>(new LocalSchedule{BUNDLE_SIZE});
    } else if (!strcmp(cscheme, "global")) {
    // we use a small integer size (order_t) for communication ranks; make sure
    // things fit for all ranks
        if (!rank && pow(2, 8*sizeof(order_t)) < g_world_size) {
            std::cerr << "Rank infos are send in too small data type for this job ("
                      << g_world_size << " v.s. "
                      << pow(2, sizeof(order_t)) << ") ...\n";
            exit(4);
        }
        const char* coption = getenv("MPITHOR_GLOBAL_DECISION_INTERVAL");
        int interval = coption ? atoi(coption) : 100;
        coption = getenv("MPITHOR_GLOBAL_DECISION_WARMUP");
        int warmup_skip = coption ? atoi(coption) : 5;
        coption = getenv("MPITHOR_GLOBAL_DECISION_REMEASURE");
        int remeasure = coption ? atoi(coption) : 1000;
        if (!rank) {
            std::cerr << "[mpithor] Analysis interval:  " << interval    << std::endl;
            std::cerr << "[mpithor] Analysis warmup:    " << warmup_skip << std::endl;
            std::cerr << "[mpithor] Analysis remeasure: " << remeasure   << std::endl;
        }
        reordering = std::unique_ptr<Reordering>(
            new GlobalSchedule{interval, warmup_skip, remeasure});
    } else {
        std::cerr << "selected unknown reordering scheme: " << cscheme << std::endl;
        exit(1);
    }

    if (!rank) {
        std::cerr << "[mpithor] Running \"" << cscheme
                  << "\" reordering scheme" << std::endl;
    }
    return reordering;
}


// base class
mpithor::Reordering::~Reordering() {}

//
// factor out issuing a bundle of messages
//
size_t mpithor::Reordering::issue_bundle(
        MessageQueue& queue, const std::vector<MessageQueue::size_type>& bundle)
{
    MPITHOR_TRACER(2);

    assert(!bundle.empty());            // make sure check is done externally
    g_callstats.add_bundle_issued(bundle.size());
    const int msgtype = queue.msgtype();
    for (auto ib : bundle) {
        mem_req_t& mr = queue.get_request(ib);
        if (msgtype == MPITHOR_ISEND_MSG) {
            MPITHOR_TRACER_MSG(5, "issue MPI_Isend");
            MPI_Isend(mr.buf, mr.count, mr.datatype,
                      mr.target, mr.tag, (MPI_Comm)mr.comm, (MPI_Request*)mr.request);
        } else if (msgtype == MPITHOR_IRECV_MSG) {
            MPITHOR_TRACER_MSG(5, "issue MPI_Irecv");
            MPI_Irecv(mr.buf, mr.count, mr.datatype,
                      mr.target, mr.tag, (MPI_Comm)mr.comm, (MPI_Request*)mr.request);
#ifdef MPICH
        } else if (msgtype == MPITHOR_ISENDC_MSG) {
            MPITHOR_TRACER_MSG(5, "issue MPIC_Isend");
            MPIC_Isend(mr.buf, mr.count, mr.datatype, mr.target, mr.tag,
                       (void*)mr.comm, (void**)mr.request, mr.errflag);
        } else if (msgtype == MPITHOR_IRECVC_MSG) {
            MPITHOR_TRACER_MSG(5, "issue MPIC_Irecv");
            MPIC_Irecv(mr.buf, mr.count, mr.datatype, mr.target,
                       mr.tag, (void*)mr.comm, (void**)mr.request);
        } else if (msgtype == MPITHOR_SENDRECVC_MSG) {
            MPITHOR_TRACER_MSG(5, "issue MPIC_Sendrecv");
            MPIC_Sendrecv(mr.buf,  mr.count,  mr.datatype,  mr.target,  mr.tag,
                          mr.tbuf, mr.tcount, mr.tdatatype, mr.tsource, mr.ttag,
                          (void*)mr.comm, (MPI_Status*)mr.request, mr.errflag);
#endif // MPICH
        }
        mr.state = r_issued;
    }

    return bundle.size();
}


//
// Simply post messages immediately, with no bundling or reordering (but does
// go through the queue).
//
int mpithor::Immediate::initialize()
{
    MPITHOR_TRACER(1);
    g_issue_policy = EI_CONTINUOUS_ISSUE;
    return MPI_SUCCESS;
}


//
// Ordered reoredering requires latency information, so request it on initialize
//
int mpithor::SimpleOrdered::initialize()
{
    MPITHOR_TRACER(1);
    return collect_timings(4 /* TODO, what? */);
}

//
// Generic issue for ordered reordering (sorting done in derived classes).
//
size_t mpithor::SimpleOrdered::issue_request_bundled(
        MessageQueue& queue, issue_policy_t policy)
{
    MPITHOR_TRACER(2);

// if current bundle does not cover a large enough window, simply return
    if (!queue.ready_for_issue(policy == all ? -1 : m_bundle_size))
        return 0;

    if (policy != all) m_bundle.resize(m_bundle_size);
    queue.get_bundle_for_issue(policy == all ? -1 : m_bundle_size, m_bundle);

    if (m_bundle.size() == 1)
        return issue_bundle(queue, m_bundle);  // no sort needed

// look up all latencies once
    latency_t latencies[MAX_STATIC_REORDER_BUNDLE_SIZE];
    latency_t injection[MAX_STATIC_REORDER_BUNDLE_SIZE];
    assert(m_bundle.size() <= MAX_STATIC_REORDER_BUNDLE_SIZE);
    int mpi_dt_size = 0; MPI_Datatype last_type = MPI_DATATYPE_NULL;
    for (size_t ib = 0; ib < m_bundle.size(); ++ib) {
        mem_req_t& mr = queue.get_request(m_bundle[ib]);
        if (last_type != mr.datatype) {
            MPI_Type_size(mr.datatype, &mpi_dt_size);
            last_type = mr.datatype;
        }
        size_t blocksize = mr.count*mpi_dt_size;
        injection[ib] = send_injection(blocksize);
        latencies[ib] = send_latency(blocksize);
    }

// now sort (simple quadratic, as max bundle size is tiny)
    sort_bundle(queue, m_bundle, latencies, injection);

// sort done, now issue
    return issue_bundle(queue, m_bundle);
}


//
// high to low ordered sort
//
void mpithor::HighLow::sort_bundle(MessageQueue&, Reordering::Bundle_t& bundle,
        latency_t* latencies, latency_t* /* injections */) {
    MPITHOR_TRACER(2);

// use trivial bubble sort as bundle sizes are small
    int n = bundle.size();
    while (n) {
        for (int ib = 1; ib < n; ++ib) {
        // sort high to low latency
            if (latencies[ib-1] < latencies[ib]) {
                std::swap(m_bundle [ib], m_bundle [ib-1]);
                std::swap(latencies[ib], latencies[ib-1]);
            }
        }
        n -= 1;
    }
}


//
// low to high ordered sort
//
void mpithor::LowHigh::sort_bundle(MessageQueue&, Reordering::Bundle_t& bundle,
        latency_t* latencies, latency_t* /* injections */) {
    MPITHOR_TRACER(2);

    int n = m_bundle.size();
    while (n) {
        for (int ib = 1; ib < n; ++ib) {
        // sort low to high latency
            if (latencies[ib-1] > latencies[ib]) {
                std::swap(m_bundle [ib], m_bundle [ib-1]);
                std::swap(latencies[ib], latencies[ib-1]);
            }
        }
        n -= 1;
    }
}


namespace mpithor {

// helper to estimate which order is more beneficial, assuming full overlap
static inline latency_t estimate(
        latency_t Tinj1, latency_t Tnet1, latency_t Tinj2, latency_t Tnet2) {
    latency_t v1 = Tinj1 + fmax(Tnet1, Tinj2 + Tnet2);
    latency_t v2 = Tinj2 + fmax(Tnet2, Tinj1 + Tnet1);

    return v1 - v2;
}

} // namespace mpithor


//
// local schedule based on estimator
//
void mpithor::LocalSchedule::sort_bundle(MessageQueue& queue, Reordering::Bundle_t& bundle,
        latency_t* latencies, latency_t* injections) {
// pull pairs of the bundle and find recursively the most beneficial pairings
    std::vector<latency_t> keys; keys.reserve(bundle.size()*(bundle.size()+1)/2);
    std::map<latency_t, std::pair<size_t, size_t>> orders;

    size_t* selector = new size_t[bundle.size()];

// collect all pairs
    for (size_t ib1 = 0; ib1 < bundle.size(); ++ib1) {
        selector[ib1] = 1;

        for (size_t ib2 = ib1+1; ib2 < bundle.size(); ++ib2) {
            latency_t est = estimate(
                injections[ib1], latencies[ib1], injections[ib2], latencies[ib2]);

            if (est < 0) {
            // means sending msg ONE first is faster: keep order
                double a_est = fabs(est);
                while (orders.find(a_est) != orders.end())
                    a_est = a_est*1.001+1E-5;
                orders[a_est] = std::make_pair(ib1, ib2);
                keys.push_back(a_est);
            } else {
            // means sending msg TWO first is faster: reverse order
                while (orders.find(est) != orders.end())
                    est = est*1.001+1E-5;
                orders[est] = std::make_pair(ib2, ib1);
                keys.push_back(est);
            }
        }
    }

// sort the keys
    std::sort(keys.rbegin(), keys.rend());
    assert(keys.size() == orders.size());

// peel of pairs until done
    Bundle_t new_bundle; new_bundle.reserve(bundle.size());
    for (auto key : keys) {
        assert(orders.find(key) != orders.end());
        const auto& order = orders[key];
        if (selector[order.first] && selector[order.second]) {
        // add to the new bundle
            new_bundle.push_back(bundle[order.first]);
            new_bundle.push_back(bundle[order.second]);
            selector[order.first] = selector[order.second] = 0;
        }

    }
    assert(new_bundle.size() == bundle.size());
    bundle.swap(new_bundle);

#ifndef NDEBUG
    for (size_t i = 0; i < bundle.size(); ++i) assert(selector[i] == 0);
#endif

    delete [] selector;
}


//
// global schedule constructor
//
mpithor::GlobalSchedule::GlobalSchedule(int interval, int warmup, int remeasure) :
        m_interval((count_t)interval), m_warmup((count_t)warmup),
        m_remeasure((count_t)remeasure) {
// create the default region, so that m_cur_analyses is always valid
    m_last_marker = DEFAULT_REGION_MARKER;
    m_cur_analyses = &m_all_analyses[DEFAULT_REGION_MARKER];
    m_cur_analysis_data = nullptr;
    m_data_ready = false;
}

//
// initialization for global schedule
//
int mpithor::GlobalSchedule::initialize()
{
    MPITHOR_TRACER(1);
    g_issue_policy = EI_DRAIN_ONLY;
    return MPI_SUCCESS;
}

//
// setup for a new analysis region
//
int mpithor::GlobalSchedule::new_region(int marker)
{
    return MPI_SUCCESS;
}

//
// reset ordering to the next application provided one and redo analysis
//
int mpithor::GlobalSchedule::reset()
{
    MPITHOR_TRACER(1);

    m_all_analyses.clear();
    m_cur_analyses = &m_all_analyses[DEFAULT_REGION_MARKER];
    m_cur_analysis_data = nullptr;
    m_data_ready = false;

    return MPI_SUCCESS;
}

//
// helper that calculates hash pairs
//
static inline void add_val_hash(uint64_t& hash, int val) {
     hash += (uint64_t)val;
     hash += (hash << 10); hash ^= (hash >> 6);
}

static inline void mix_bits(uint64_t& hash) {
     hash += (hash << 3); hash ^= (hash >> 11); hash += (hash << 15);
}

mpithor::GlobalSchedule::AnalysisKey_t mpithor::GlobalSchedule::get_key(
        MessageQueue& queue, const Reordering::Bundle_t& bundle)
{
    uint64_t size_hash = 0, rank_hash = 0;
    int mpi_dt_size = 0; MPI_Datatype last_type = MPI_DATATYPE_NULL;
    for (auto ib : m_bundle) {
        mem_req_t& mr = queue.get_request(ib);
        if (last_type != mr.datatype) {
            MPI_Type_size(mr.datatype, &mpi_dt_size);
            last_type = mr.datatype;
        }

        add_val_hash(size_hash, mr.count*mpi_dt_size);
        add_val_hash(rank_hash, mr.target);
    }

    mix_bits(size_hash); mix_bits(rank_hash);

#ifndef NDEBUG
    if (MPITHOR_VERBOSE && GET_MYRANK() == MPITHOR_ANALYSIS_RANK)
            std::cerr << "[mpithor:" << MPITHOR_ANALYSIS_RANK
                      << "] using analysis key (" << size_hash << ", " << rank_hash
                      << ")\n";
#endif

    return  AnalysisKey_t{size_hash, rank_hash};
}

//
// helper that reorders messages
//
void mpithor::GlobalSchedule::reorder(std::vector<MessageQueue::size_type>& bundle,
        const AnalysisData& ana)
{
    assert(bundle.size() == ana.m_order_mapping.size());
    std::vector<MessageQueue::size_type> reordered; reordered.resize(bundle.size());
    for (size_t i = 0; i < m_bundle.size(); ++i) {
        assert(ana.m_order_mapping[i].first < reordered.size());
        reordered[ana.m_order_mapping[i].first] = bundle[i];
    }
    bundle.swap(reordered);
}

//
// global consensus schedule
//
size_t mpithor::GlobalSchedule::issue_request_bundled(
        MessageQueue& queue, issue_policy_t policy )
{
    MPITHOR_TRACER(2);
    assert(g_state != EO_DISABLED);

// no issue if no messages ...
    if (!queue.ready_for_issue()) {
        MPITHOR_TRACER_MSG(3, "[global schedule] no messages");
        return 0;
    }

// wait until a sync forces full drain
    if (policy != all) {
        MPITHOR_TRACER_MSG(3, "[global schedule] no drain");
        return 0;
    }

// short-circuit (w/o reordering) schedule if not active
    if (g_state == EO_ANALYSIS_OFF) {
        MPITHOR_TRACER_MSG(3, "[global schedule] drain bundle, no analysis");
        queue.get_bundle_for_issue(-1 /* all */, m_bundle);
        return issue_bundle(queue, m_bundle);
    }

// prepare for analysis
    MPITHOR_TRACER_MSG(3, "[global schedule] drain bundle");
    queue.get_bundle_for_issue(-1 /* all */, m_bundle);

// don't bother if no ordering possible
    if (m_bundle.size() <= 1) {
        MPITHOR_TRACER_MSG(3, "[global schedule] bundle too small, no analysis");
        return issue_bundle(queue, m_bundle);
    }

// the analysis done is per hash(sizes) hash(ranks) pairs for the statistics
// to make sense; which does mean that the behavior across all nodes needs to be
// similar (i.e.switch-over at the same time) or dead-lock will occur
    AnalysisKey_t cur_key = MPITHOR_KEYED_ANALYSIS ? get_key(queue, m_bundle) : AnalysisKey_t{0, 0};
    assert(m_cur_analyses);
    auto pit = m_cur_analyses->find(cur_key);
    if (pit == m_cur_analyses->end()) {
        auto res = m_cur_analyses->emplace(cur_key,
            AnalysisData{m_warmup, std::max(m_interval, m_remeasure), m_bundle, queue});
        assert(res.second);
        pit = res.first;
    }
    m_cur_analysis_data = &pit->second;

#ifndef NDEBUG
// make sure we always see the same size bundle, otherwise we are not
// dealing with a fixed domain decomposition
    assert(m_cur_analysis_data->m_order_mapping.size() == m_bundle.size());

// likewise, verify that the neigbors haven't changed
    for (auto ib : m_bundle) {
        mem_req_t& mr = queue.get_request(ib);
        bool found = false;
        for (auto p : m_cur_analysis_data->m_order_mapping) {
            if (p.second == mr.target) {
                found = true;
                break;
            }
        }
        assert(found == true);
    }
#endif

// reorder and issue (recv's are treated differently as they are small and
// can't reliably drive the ordering decisions)
    if (is_recv_type(queue.msgtype())) {
    // handle receives
        if (MPITHOR_REORDER_RECVS) reorder(m_bundle, *m_cur_analysis_data);
    } else if (is_send_type(queue.msgtype())) {
        reorder(m_bundle, *m_cur_analysis_data);
    } else {
        assert(!"unknown message type!");
    }

    m_cur_analysis_data->m_clock_start = AnalysisData::clock_t::now();  
    return issue_bundle(queue, m_bundle);
}


// helper to factor out readiness check
static inline bool analysis_is_ready(mpithor::AnalysisData& ana, size_t interval)
{
    return ana.m_state != mpithor::AnalysisData::EA_STOPPED && \
        interval <= ana.m_comm_times.size();
}

static inline size_t block_sz(size_t rounds, size_t analyses)
{
    return sizeof(float)*analyses       // average latency
         + sizeof(float)*analyses       // standard deviation
         + sizeof(order_t)*analyses     // N = number of rounds
         + sizeof(order_t)*rounds;      // rank in each round
}

int mpithor::GlobalSchedule::communication_done(int message_type)
{
    MPITHOR_TRACER(2);

// stop timer (not always used later, though)
    auto clock_stop = AnalysisData::clock_t::now();

    if (!m_cur_analysis_data)     // e.g. if skipped b/c bundle too small
        return MPI_SUCCESS;

// complete message gathering and run analysis as needed
    if (m_cur_analysis_data->m_skip) {
    // warmup is skipped completely (by definition), remeasuring over the larger
    // skip region theoretically should give better statistics, but if there is a
    // trend, it screws things up, so just fully ignore
        m_cur_analysis_data->m_skip -= 1;
        m_cur_analysis_data = nullptr;
        return MPI_SUCCESS;
    } else {
    // done with skip, get going afresh
        if (m_cur_analysis_data->m_state == AnalysisData::EA_REMEASURE) {
            m_cur_analysis_data->m_state = AnalysisData::EA_GLOBAL;
            m_cur_analysis_data->m_best_average = FLT_MAX;   // "forces" do-over
        }
    }

// save timing result for use in analysis
    auto lapsed_time = clock_stop - m_cur_analysis_data->m_clock_start;
    m_cur_analysis_data->m_comm_times.push_back(
        std::chrono::duration_cast<std::chrono::microseconds>(lapsed_time).count());

    if (analysis_is_ready(*m_cur_analysis_data, m_interval))
        m_data_ready = true;

    m_cur_analysis_data = nullptr;

    if (!MPITHOR_ANALYSIS_IN_BARRIER && !MPITHOR_ANALYSIS_IN_ALLREDUCE)
        analyze();

    return MPI_SUCCESS;
}


bool mpithor::GlobalSchedule::analyze()
{
    MPITHOR_TRACER(2);

// timing already taken in communication_end()
    assert(!m_cur_analysis_data);

// run_analysis only if sufficient data was collected
    if (!m_data_ready)
        return false;
 
// start clocking (may go unused)
    auto analysis_time_start = AnalysisData::clock_t::now();

    MPITHOR_TRACER_MSG(1, "analysis start");

// just in case any of the MPI functions used here recurses
    mpithor_start_pause();

// determine the proper buffer size
    size_t all_rounds = 0, num_analyses = 0;
    for (auto& p : m_all_analyses[m_last_marker]) {
        AnalysisData& ana = p.second;

    // analyze only if active and have sufficient data
        if (!(analysis_is_ready(ana, m_interval)))
            continue;

    // mark analysis for use (can not rely on state as it changed during
    // the analysis for the analysis rank)
        ana.m_is_active = true;

    // just once per analysis, determine number of communication rounds
        if (ana.m_max_rounds == 0) {
            int my_max_rounds = (int)ana.m_order_mapping.size(), recv_max_rounds = 0;
        // as usual, this assumes full SPMD, so all reduce here is safe
            MPI_Allreduce(&my_max_rounds, &recv_max_rounds, 1, MPI_INT,
                          MPI_MAX, MPI_COMM_WORLD);
            ana.m_max_rounds = recv_max_rounds;
            assert(ana.m_max_rounds);

        // first time, initialize best order to "no change mapping"
            ana.m_order.reserve(recv_max_rounds);
            for (int i = 0; i < recv_max_rounds; ++i)
                ana.m_order.push_back(i);
        }

        all_rounds += ana.m_max_rounds;
        num_analyses += 1;
    }

    const size_t SENDBUFSIZE = block_sz(all_rounds, num_analyses)
#ifndef NDEBUG
                             + 2*sizeof(uint32_t)* num_analyses  // protectors
#endif
                               ;
    char* sendbuf = new char[SENDBUFSIZE];

// collect all data for all analyses under the last/current marker
    char* fillbuf = sendbuf;
    for (auto& p : m_all_analyses[m_last_marker]) {
        AnalysisData& ana = p.second;

    // analyze only if active and have sufficient data
        if (!ana.m_is_active)
            continue;

        assert(ana.m_max_rounds);

    // calculate local average and stddev
        double avg_latency = std::accumulate(std::begin(ana.m_comm_times),
            std::end(ana.m_comm_times), 0.)/(double)ana.m_comm_times.size();
        double stddev = 0.;
        for (auto val : ana.m_comm_times) {
            float s = val - avg_latency;
            stddev += s*s;
        }
        double divisor = ana.m_comm_times.size() == 1 ? 1 : ana.m_comm_times.size() - 1;
        stddev = sqrt(stddev/divisor);

    // add these average time, stddev, and order used to the analysis rank
#ifndef NDEBUG
        *((uint32_t*)fillbuf) = 0xCDCDCDCD;
        fillbuf += sizeof(uint32_t);
#endif
        *((float*)fillbuf)   = avg_latency;
        *((float*)fillbuf+1) = stddev;
        order_t* ordbuf = (order_t*)((float*)fillbuf+2);
        ordbuf[0] = (order_t)ana.m_order_mapping.size();
        for (size_t i = 0; i < ana.m_order_mapping.size(); ++i) {
            assert(ana.m_order_mapping[i].first < ana.m_order_mapping.size());
            ordbuf[ana.m_order_mapping[i].first+1] = (order_t)ana.m_order_mapping[i].second;
        }

    // offset fillbuf for the next analysis
        fillbuf += block_sz(ana.m_max_rounds, 1);
#ifndef NDEBUG
        *((uint32_t*)fillbuf) = 0x32323232;
        fillbuf += sizeof(uint32_t);
#endif
    }

    assert(fillbuf-sendbuf == (ptrdiff_t)SENDBUFSIZE);

// perform exchange
    char* recvbuf = nullptr;
    if (g_world_rank == MPITHOR_ANALYSIS_RANK)
        recvbuf = new char[SENDBUFSIZE*g_world_size];

    MPI_Gather(sendbuf, SENDBUFSIZE, MPI_CHAR, recvbuf, SENDBUFSIZE, MPI_CHAR,
        MPITHOR_ANALYSIS_RANK, MPI_COMM_WORLD);

// actual analysis starts here, with only the analysis rank processing
// (TODO: analyze on measured fastest node)
    if (g_world_rank == MPITHOR_ANALYSIS_RANK){

        fillbuf = sendbuf;
        char* readbuf = recvbuf;
        for (auto& p : m_all_analyses[m_last_marker]) {
            AnalysisData& ana = p.second;

        // analyze only if active and have sufficient data
            if (!ana.m_is_active)
                continue;

            assert(ana.m_max_rounds);

        // calculate local average and stddev
            bool need_reversal = false;

        // collect all info
            std::vector<float> avg_times; avg_times.reserve(g_world_size);
            std::vector<float> read_sd_times; read_sd_times.reserve(g_world_size);
            std::vector<std::vector<size_t>> read_orderings{(size_t)g_world_size};

            size_t max_neighbors = 0;
            for (int inode = 0; inode < g_world_size; ++inode) {
                char* node_addr = readbuf+SENDBUFSIZE*inode;
#ifndef NDEBUG
                assert(*((uint32_t*)node_addr) == 0xCDCDCDCD);
                node_addr += sizeof(uint32_t);
#endif
                avg_times.push_back(*((float*)node_addr));
                read_sd_times.push_back(*((float*)node_addr+1));

                order_t* recvord = (order_t*)((float*)node_addr+2);
                size_t nn = (size_t)recvord[0];
                max_neighbors = std::max(max_neighbors, nn);
                auto& v = read_orderings[inode]; v.reserve(nn);
                for (size_t s = 0; s < nn; ++s)
                    v.push_back(recvord[s+1]);
#ifndef NDEBUG
                assert(nn == ana.m_max_rounds);
                node_addr += block_sz(nn, 1);
                assert(*((uint32_t*)node_addr) == 0x32323232);
#endif
            }

        // prepare for next round
            readbuf += block_sz(ana.m_max_rounds, 1);
#ifndef NDEBUG
            readbuf += 2*sizeof(uint32_t);
#endif

        // check for slowest (needed for history)
#ifndef NDEBUG
            size_t slowest_node = (size_t)-1;
#endif
            float slowest_time = -1.f;
            for (size_t i = 0; i < avg_times.size(); ++i) {
                if (slowest_time < avg_times[i]) {
                    slowest_time = avg_times[i];
#ifndef NDEBUG
                    slowest_node = i;
#endif
                }
            }

        // move on depending on whether we improved or not
            if (slowest_time < ana.m_best_average) {
                g_callstats.add_won();
#ifndef NDEBUG
                if (MPITHOR_VERBOSE) {
                    fprintf(stderr, "[mpithor:%d] Won! Old: %g; new: %g (%lu is slowest [%d])\n",
                        MPITHOR_ANALYSIS_RANK, ana.m_best_average, slowest_time, slowest_node,
                        g_current_region);
                }
#endif
                ana.m_best_average = slowest_time;

            // typically, only one local round is needed (TODO: tune)
                if (ana.m_state == AnalysisData::EA_LOCAL)
                    ana.m_state = AnalysisData::EA_GLOBAL;

            // save orderings for current use
                ana.m_orderings = std::move(read_orderings);

            // average times are always current (no save)

            // save sd_times for possible use next round, as well as the
            // currently slowest non-edge node
                ana.m_sd_times = std::move(read_sd_times);
            
                float non_edge_slowest_time = -1.f;
                for (size_t i = 0; i < avg_times.size(); ++i) {
                    if (non_edge_slowest_time < avg_times[i] &&         \
                            ana.m_orderings[i].size() == max_neighbors) {
                        non_edge_slowest_time = avg_times[i];
                        ana.m_slowest_node = i;
                    }
                }

            } else {     // regression
                g_callstats.add_lost();
#ifndef NDEBUG
                if (MPITHOR_VERBOSE) {
                    std::cerr << "[mpithor:" << MPITHOR_ANALYSIS_RANK << "] Lost... "
                              << ana.m_best_average << " " << slowest_time << " ("
                              << g_current_region << ") ... \n";
                }
#endif

                if (ana.m_state == AnalysisData::EA_GLOBAL) {
                    ana.m_state = AnalysisData::EA_LOCAL;

                // use stddevs from previous round for this analysis (no change)
                // likewise, restore old order for analysis (no change)
                } else if (ana.m_state == AnalysisData::EA_LOCAL) {
                    ana.m_state = AnalysisData::EA_STOPPED;
                // restore old order (no change)
                } else
                    assert(0);
#ifndef NDEBUG
                read_orderings.clear();
                read_sd_times.clear();
#endif
            }

#ifndef NDEBUG
            if (ana.m_state != AnalysisData::EA_STOPPED) {
                assert(read_orderings.empty() && read_sd_times.empty());
                assert(!ana.m_orderings.empty() && !ana.m_sd_times.empty());
            }
#endif

            if (ana.m_state == AnalysisData::EA_GLOBAL) {
                MPITHOR_TRACER_MSG(2, "global analysis");
                g_callstats.add_global();

            // calculate weighted sums; allow only the slowest 50% of nodes to vote; exclude
            // nodes that do not have a full complement of communication partners
                std::vector<std::pair<float, size_t>> votes; votes.resize(max_neighbors);
                for (size_t i = 0; i < votes.size(); ++i) {
                    votes[i].first  = 0.f;
                    votes[i].second = i;
                }

                std::vector<std::pair<float, size_t>> sorted_t; sorted_t.reserve(avg_times.size());
                for (size_t i = 0; i < avg_times.size(); ++i)
                    sorted_t.push_back(std::make_pair(avg_times[i], i));
                std::sort(sorted_t.rbegin(), sorted_t.rend());
		
                for (size_t i = 0; i < sorted_t.size()/2; ++i) {     // select slowest 50%
                    size_t voting_node = sorted_t[i].second;
                    const auto& order = ana.m_orderings[voting_node];
                    if (order.size() == max_neighbors) {             // skip edge nodes
                        for (size_t iround = 0; iround < max_neighbors; ++iround) {
                            assert(order[iround] < avg_times.size());
                            votes[iround].first += avg_times[order[iround]];
                        }
                    }	
                }
		
            // sort rounds by weighted value, sort by votes[i].first
                std::sort(votes.rbegin(), votes.rend());

            // save new order as a permutation on the old one
                assert(ana.m_order.size() == max_neighbors);
                for (size_t i = 0; i < max_neighbors; ++i)
                    ana.m_order[i] = votes[i].second;

            } else if (ana.m_state == AnalysisData::EA_LOCAL) {
                MPITHOR_TRACER_MSG(2, "local analysis");
                g_callstats.add_local();

#ifndef NDEBUG
            // check for slowest, non-edge only
                if (MPITHOR_VERBOSE) {
                    std::cerr << "[mpithor:" << MPITHOR_ANALYSIS_RANK
                              << "] Running LOCAL analysis for node: "
                              << ana.m_slowest_node << " (" << g_current_region << ") ... \n";
                }
#endif

            // retrieve order of slowest node and sort
                const auto& slowest_order = ana.m_orderings[ana.m_slowest_node];
                if (slowest_order.size() != max_neighbors)
                    std::cerr << slowest_order.size() << " " << max_neighbors << std::endl;
                assert(slowest_order.size() == max_neighbors);

            // vote based on standard deviation
                std::vector<std::pair<float, size_t>> votes;
                votes.resize(slowest_order.size());
                for (size_t iround = 0; iround < slowest_order.size(); ++iround) {
                    assert(slowest_order[iround] < ana.m_sd_times.size());
                    votes[iround].first = ana.m_sd_times[slowest_order[iround]];
                    votes[iround].second = iround;   //TODO error!!!! should be target not iround
                }

            // sort and save
                std::sort(votes.rbegin(), votes.rend());

            // we worked on info from a previous round, so require a reversal before
            // applying new order
                need_reversal = true;

            // save new order
                assert(ana.m_order.size() == max_neighbors);
                for (size_t i = 0; i < max_neighbors; ++i)
                    ana.m_order[i] = votes[i].second;

            } else if (ana.m_state == AnalysisData::EA_STOPPED) {
                if (ana.m_best_average<= slowest_time) {
                // got here b/c of overshoot: restore order
#ifndef NDEBUG
                    if (MPITHOR_VERBOSE)
                        std::cerr << "[mpithor:" << MPITHOR_ANALYSIS_RANK
                                  << "] Analysis STOPPED and REVERTING!\n";
#endif
                    need_reversal = true;

                // the reversal restores; for simplicity, apply a no-op reorder
                    assert(ana.m_order.size() == ana.m_max_rounds);
                    for (size_t i = 0; i < max_neighbors; ++i)
                        ana.m_order[i] = i;
                }
#ifndef NDEBUG
                else {
                    if (MPITHOR_VERBOSE)
                        std::cerr << "[mpithor:" << MPITHOR_ANALYSIS_RANK << "] Analysis STOPPED\n";
                }
#endif
            } else {
                assert(0);
            }

#ifndef NDEBUG
            if (MPITHOR_VERBOSE) {
                std::cerr << "[mpithor:" << MPITHOR_ANALYSIS_RANK << "] new order: ";
                for (size_t i = 0; i < ana.m_order.size(); i++)
                    std::cerr << ana.m_order[i] << " ";
                std::cerr << std::endl;
            }
#endif

        // store back updated ordering for other nodes to receive (reuse sendbuf,
        // through fillbuf, as it is easily large enough)
            assert(sizeof(order_t) == 2*sizeof(uint8_t));
            *((uint8_t*)fillbuf  ) = (uint8_t)ana.m_state;
            *((uint8_t*)fillbuf+1) = (uint8_t)need_reversal;
            *((order_t*)fillbuf+1) = (order_t)ana.m_order.size();
            order_t* ordbuf = (order_t*)fillbuf+2;
            for (size_t i = 0; i < ana.m_order.size(); ++i)
                ordbuf[i] = (order_t)ana.m_order[i];
            assert(ana.m_order.size() == max_neighbors);

        // offset for next analysis
            fillbuf += 2*sizeof(uint8_t)+sizeof(order_t)*(1+ana.m_order.size());
        }
    }

// done with recvbuf
    delete [] recvbuf; recvbuf = nullptr;

// broadcast analysis results, next steps, and new order (TODO: isn't the total
// size fixed anyway, and can thus use a smaller buffer than sendbuf?)
    MPI_Bcast(sendbuf, SENDBUFSIZE, MPI_CHAR, MPITHOR_ANALYSIS_RANK, MPI_COMM_WORLD);

    char* readbuf = sendbuf;
    for (auto& p : m_all_analyses[m_last_marker]) {
        AnalysisData& ana = p.second;

    // analyze only if active and have sufficient data
        if (!ana.m_is_active)
            continue;

    // this is the last loop ... done with current set of measurements
        ana.m_comm_times.clear();
        ana.m_is_active = false;

    // new state
#ifndef NDEBUG
        if (g_world_rank == MPITHOR_ANALYSIS_RANK) {
            assert(ana.m_state == (AnalysisData::AnalysisState)*((uint8_t*)readbuf));
            assert(*((order_t*)readbuf+1) == (order_t)ana.m_order.size());
        }
#endif
        ana.m_state = (AnalysisData::AnalysisState)*((uint8_t*)readbuf);

    // apply reversal as appropriate
        if (*((uint8_t*)readbuf+1) /* need_reversal */ ) {
            assert(!ana.m_last_order_mapping.empty());
            ana.m_order_mapping = ana.m_last_order_mapping;
        } else {              // no reversal: store history before reordering
            assert(!ana.m_order_mapping.empty());
            ana.m_last_order_mapping = ana.m_order_mapping;
        }

    // apply the newly given order as a permutation
        size_t norder = *((order_t*)readbuf+1);
        assert(!norder || ana.m_order_mapping.size() <= norder);
        order_t* ordbuf = (order_t*)readbuf+2;
        for (auto& p : ana.m_order_mapping) {
            size_t ioffset = 0;
            for (size_t i = 0; i < norder; ++i) {
                size_t newpos = ordbuf[i];
                if (ana.m_order_mapping.size() <= newpos) {
                    ioffset += 1;  // position to skip
                    continue;
                }
                if (p.first == newpos) {
                    p.first = i-ioffset;
                    break;
                }
            }
        }

        readbuf += 2*sizeof(uint8_t)+sizeof(order_t)*(1+norder);

#ifndef NDEBUG
    // check that all ordered messages occur once and only once
        std::vector<size_t> verifier(ana.m_order_mapping.size());
        for (auto& p : ana.m_order_mapping)
            verifier[p.first] = 1;

        for (auto& i : verifier) {
            if (i != 1) {
                std::cerr << GET_MYRANK() << " ORDERING FAILED "
                          << ana.m_order_mapping.size() << " " << norder << " ";
                for (size_t i = 0; i < norder; ++i) {
                    std::cerr << ordbuf[i] << " ";
                }
                std::cerr << std::endl;
            }
            assert(i == 1);
        }
#endif

    // now reset skip period to allow remeasurement
        if (ana.m_state == AnalysisData::EA_STOPPED && m_remeasure) {
            ana.m_skip  = m_remeasure;
            ana.m_state = AnalysisData::EA_REMEASURE;
            if (MPITHOR_VERBOSE && GET_MYRANK() == MPITHOR_ANALYSIS_RANK)
                std::cerr << "[mpithor:" << MPITHOR_ANALYSIS_RANK
                          << "] Will remeasure in " << ana.m_skip << " rounds\n";
        }
    }

// done with sendbuf
    delete [] sendbuf;

// done with analysis
    auto analysis_time_stop = AnalysisData::clock_t::now();

    g_callstats.add_analysis(
        std::chrono::duration_cast<std::chrono::microseconds>(
            analysis_time_stop-analysis_time_start).count());

    m_data_ready = false;     // done with current data
    mpithor_end_pause();
    return true;
}
