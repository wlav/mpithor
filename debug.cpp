#include "mpithor/config.h"

#define MPITHOR_INTERNAL 1
#include "mpithor/smpi.h"
#include "mpithor/debug.h"

int GET_MYRANK() {
    static int myrank = -1;
    if (myrank < 0)
        MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    return myrank;
}
