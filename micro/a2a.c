#include "mpithor/smpi.h"
#include "mpithor/mpithor.h"
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <sys/time.h>

#define TIME_SPENT(start, end) \
    (end.tv_sec * 1000000. + end.tv_usec - start.tv_sec*1000000. - start.tv_usec)

#ifndef EXIT_SUCCESS
#define EXIT_SUCCESS 0
#define EXIT_FAILURE 1
#endif

static int STAT_OFFSET = 50;


int main(int argc, char *argv[])
{
    int rank, size;
    int chunk = 128;
    int repeats = 1000;
    int irep, i, recv_total, exp_total;
    int *sb;
    int *rb;
    int status, gstatus;
    struct timeval tstart, tstop, tfullstart, tfullstop;
    float duration, gduration;
    float first_avg, last_avg;
#ifndef NDEBUG
    int db1 = 0, db2 = 0;
#endif

    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Comm_size(MPI_COMM_WORLD,&size);
    for (i=1 ; i < argc ; ++i) {
        if (argv[i][0] != '-')
            continue;
        switch(argv[i][1]) {
            case 'm':
                chunk = atoi(argv[++i]);
                if (rank == 0) fprintf(stderr, "chunk size: %ld bytes\n", chunk*sizeof(int));
                break;
            case 'r':
                repeats = atoi(argv[++i]);
                if (repeats < 150) {
                    STAT_OFFSET = 0;
                    if (rank == 0) fprintf(stderr,
                            "WARNING: Too few repeats (%d) for proper statistics\n",
                            repeats);
                }
                break;
            default:
                fprintf(stderr, "Unrecognized argument %s\n", argv[i]);fflush(stderr);
                MPI_Abort(MPI_COMM_WORLD,EXIT_FAILURE);
        }
    }
    sb = (int*)malloc(size*chunk*sizeof(int));
    if (!sb) {
        perror("can't allocate send buffer");
        MPI_Abort(MPI_COMM_WORLD,EXIT_FAILURE);
    }
    rb = (int*)malloc(size*chunk*sizeof(int));
    if (!rb) {
        perror("can't allocate recv buffer");
        free(sb);
        MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
    }

#ifndef DISABLE_MPITHOR
    mpithor_start_pause();
#endif

// run 10 dummy alltoall/allreduce to get anything initialized that needs to be
    for (irep = 0; irep < 10; ++irep) {
        status = MPI_Alltoall(sb, chunk, MPI_INT, rb, chunk, MPI_INT, MPI_COMM_WORLD);
        MPI_Allreduce(&status, &gstatus, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
        if (gstatus != MPI_SUCCESS) {
           fprintf(stderr, "Failed initial alltoall\n");
           exit(5);
        }
    }

    gettimeofday(&tfullstart, NULL);

    first_avg = last_avg = 0.;
    for (irep = 0; irep < repeats+STAT_OFFSET; ++irep) {
        for (i = 0; i < size*chunk ; ++i) {
            sb[i] = rank + 1;
            rb[i] = 0;
        }

#ifndef DISABLE_MPITHOR
        if (irep == STAT_OFFSET)
            mpithor_end_pause();
#endif

        MPI_Barrier(MPI_COMM_WORLD);
        gettimeofday(&tstart, NULL);
        status = MPI_Alltoall(sb, chunk, MPI_INT, rb, chunk, MPI_INT, MPI_COMM_WORLD);
        gettimeofday(&tstop, NULL);
        MPI_Allreduce(&status, &gstatus, 1, MPI_INT, MPI_SUM, MPI_COMM_WORLD);
        if (gstatus != 0) {
            fprintf(stderr, "all_to_all returned %d\n",gstatus);
        } else {
            duration = (float)TIME_SPENT(tstart, tstop);
            MPI_Reduce(&duration, &gduration, 1, MPI_FLOAT, MPI_MAX, 0, MPI_COMM_WORLD);

            recv_total = exp_total = 0;
            for (i=0 ; i < size*chunk ; ++i)
                recv_total += rb[i];

            for (i=0; i<size; ++i) exp_total += chunk*(i+1);
            assert(recv_total == exp_total);

            if (irep < STAT_OFFSET) {
                first_avg += gduration;
#ifndef NDEBUG
                db1 += 1;
#endif
            } else if (repeats <= irep) {
                last_avg += gduration;
#ifndef NDEBUG
                db2 += 1;
#endif
            }

        //  if (rank == 0 && STAT_OFFSET <= irep)
        //      fprintf(stderr, "(%d) time: %f\n", irep-STAT_OFFSET, gduration);
        }
    }

    assert(db1 == STAT_OFFSET);
    assert(db2 == STAT_OFFSET);

    if (STAT_OFFSET && rank == 0)
        fprintf(stderr, "Unordered: %f\nOrdered: %f\n", first_avg/STAT_OFFSET, last_avg/STAT_OFFSET);

    free(sb);
    free(rb);

    gettimeofday(&tfullstop, NULL);
    duration = (float)TIME_SPENT(tfullstart, tfullstop);
    MPI_Reduce(&duration, &gduration, 1, MPI_FLOAT, MPI_MAX, 0, MPI_COMM_WORLD);
    if (rank == 0)
        fprintf(stderr, "end-to-end: %f\n", gduration);

    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return(EXIT_SUCCESS);
}
