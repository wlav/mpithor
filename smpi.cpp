#include "mpithor/config.h"

#define MPITHOR_INTERNAL 1
#include "mpithor/debug.h"
#include "mpithor/internal.h"
#include "mpithor/callstats.h"
#include "mpithor/smpi.h"
#include "mpithor/mpithor.h"
#include "mpithor/MessageQueue.h"
#include "mpithor/Reordering.h"

using namespace mpithor;


//
// Message queues: each message is queued indepedently to allow sorting on sends and make
// the recvs follow suit. This precludes intermixing.
//
MessageQueue g_msg_queue[] = {
    MessageQueue(MPITHOR_ISEND_MSG),     MessageQueue(MPITHOR_IRECV_MSG),
    MessageQueue(MPITHOR_ISENDC_MSG),    MessageQueue(MPITHOR_IRECVC_MSG),
    MessageQueue(MPITHOR_SENDRECVC_MSG)
};


//
// Initialize reordering run-time right after MPI has been setup and communication has become
// possible (allowing the reordering init to call MPI).
//
int MPITHOR_DECL_WRAP(MPI_Init)(int *argc, char ***argv)
{
    MPITHOR_TRACER(1);
    int result = MPI_Init(argc, argv);
    if (result != MPI_SUCCESS)
        return result;
    return mpithor_initialize();
}

int MPITHOR_DECL_WRAP(MPI_Init_thread)(int *argc, char ***argv, int required, int *provided)
{
    MPITHOR_TRACER(1);
    int result = MPI_Init_thread(argc, argv, required, provided);
    if (result != MPI_SUCCESS)
        return result;
    return mpithor_initialize();
}


//
// Close-out reordering, which needs to make sure that no further MPI gets called.
//  
int MPITHOR_DECL_WRAP(MPI_Finalize)()
{
    MPITHOR_TRACER(1);
    mpithor_finalize();
    return MPI_Finalize();
}

//
// sMPI_Isend, sMPI_Irecv
//
// Queue up message requests in their respective queues.
//  
int MPITHOR_DECL_WRAP(MPI_Isend)(const void *buf, int count, MPI_Datatype datatype, int dest,
               int tag, MPI_Comm comm, MPI_Request *request)
{
    MPITHOR_TRACER(1);
    if (mpithor::g_state == mpithor::EO_DISABLED)
        return MPI_Isend(buf, count, datatype, dest, tag, comm, request);
    mpithor::g_callstats.add_send();
    MessageQueue& queue = g_msg_queue[MPITHOR_ISEND_MSG];
    queue.enqueue_request((void*)buf, count, datatype, dest, tag, (ptrdiff_t)comm, request);
    if (g_issue_policy == EI_CONTINUOUS_ISSUE)
        Reordering::get()->issue_request_bundled(queue, fixed_count);
    return MPI_SUCCESS;
}

int MPITHOR_DECL_WRAP(MPI_Irecv)(void *buf, int count, MPI_Datatype datatype, int source,
               int tag, MPI_Comm comm, MPI_Request *request)
{
    MPITHOR_TRACER(1);
    if (mpithor::g_state == mpithor::EO_DISABLED)
        return MPI_Irecv(buf, count, datatype, source, tag, comm, request);
    mpithor::g_callstats.add_recv();
    MessageQueue& queue = g_msg_queue[MPITHOR_IRECV_MSG];
    queue.enqueue_request((void*)buf, count, datatype, source, tag, (ptrdiff_t)comm, request);
    if (g_issue_policy == EI_CONTINUOUS_ISSUE)
        Reordering::get()->issue_request_bundled(queue, fixed_count);
    return MPI_SUCCESS;
}


//  
// sMPI_Wait, sMPI_Waitall
//  
// Queue up message requests in their respective queues.
//  
int MPITHOR_DECL_WRAP(MPI_Wait)(MPI_Request *request, MPI_Status *status)
{
// TODO: think this through ... at the very least we most remove the request from the relevant
// queue, but may also require the corresponding Isend or Irecv to be issued. Worse, this may
// be used when mixing non-blocking with blocking MPI, which isn't handled properly at all.
    MPITHOR_TRACER(1);
    if (mpithor::g_state == mpithor::EO_DISABLED)
        return MPI_Wait(request, status);
    mpithor::g_callstats.add_wait();
    std::cerr << "MPI_Wait logic not suppored" << std::endl;
    exit(2);
    return MPI_Wait(request, status);
}

int MPITHOR_DECL_WRAP(MPI_Waitall)(int count, MPI_Request array_of_requests[], MPI_Status array_of_statuses[])
{
// TODO: could match up actual requrests, but for simplicity, assume Waitall matches all that
// is currently in the queue.
    MPITHOR_TRACER(1);
    if (mpithor::g_state == mpithor::EO_DISABLED)
        return MPI_Waitall(count, array_of_requests, array_of_statuses);
    mpithor::g_callstats.add_waitall();

    const std::shared_ptr<mpithor::Reordering>& reorder = mpithor::Reordering::get();

// post all remaining irecvs
    if (g_msg_queue[MPITHOR_IRECV_MSG].ready_for_issue())
        reorder->issue_request_bundled(g_msg_queue[MPITHOR_IRECV_MSG], all);

// post all remaining isends
    if (g_msg_queue[MPITHOR_ISEND_MSG].ready_for_issue())
        reorder->issue_request_bundled(g_msg_queue[MPITHOR_ISEND_MSG], all);

// actual wait (both recv and send)
// TODO: pick out the send requests and wait on them first?
    int result = MPI_Waitall(count, array_of_requests, array_of_statuses);
    if (result != MPI_SUCCESS)
        return result;

// finish measurements/analysis and its analysis (not needed for receives)
    if (g_state != EO_ANALYSIS_OFF)
        result = reorder->communication_done(MPITHOR_ISEND_MSG);
    if (g_state == EO_ANALYSIS_PENDING)
        g_state = EO_ANALYSIS_OFF;

// complete queue (TODO: tie this to the actual messages)
    std::vector<mpithor::MessageQueue::size_type> bundle;
    mpithor::MessageQueue& rq = g_msg_queue[MPITHOR_IRECV_MSG];    
    rq.get_bundle_for_completion(-1, bundle);
    for (auto ib : bundle) {
        assert(rq.get_request(ib).state == r_issued);
        rq.get_request(ib).state = r_completed;
    }
    bundle.clear();
    mpithor::MessageQueue& sq = g_msg_queue[MPITHOR_ISEND_MSG];
    sq.get_bundle_for_completion(-1, bundle);
    for (auto ib : bundle) {
        assert(sq.get_request(ib).state == r_issued);
        sq.get_request(ib).state = r_completed;
    }

    return result;
}


//
// replacement for barriers to allow Reordering analysis to run in overlap
//
int MPITHOR_DECL_WRAP(MPI_Barrier)(MPI_Comm comm)
{
    MPITHOR_TRACER(1);
    if (comm != MPI_COMM_WORLD || mpithor::g_state == mpithor::EO_DISABLED)
        return MPI_Barrier(comm);
    mpithor::g_callstats.add_barrier();
    bool synced = false;
    if (mpithor::MPITHOR_ANALYSIS_IN_BARRIER) {
        synced = mpithor::Reordering::get()->analyze();
        if (synced)
            mpithor::g_callstats.add_in_barrier_analysis();
    }

    if (!synced)
        return MPI_Barrier(comm);

    return MPI_SUCCESS;
}

//
// replacement for barriers to allow Reordering analysis to run in overlap
//
int MPITHOR_DECL_WRAP(MPI_Allreduce)(void* send, void* recv, int count, MPI_Datatype datatype,
                   MPI_Op op, MPI_Comm comm)
{
    MPITHOR_TRACER(1);
    if (comm != MPI_COMM_WORLD || mpithor::g_state == mpithor::EO_DISABLED)
        return MPI_Allreduce(send, recv, count, datatype, op, comm);
    mpithor::g_callstats.add_allreduce();
    if (mpithor::MPITHOR_ANALYSIS_IN_ALLREDUCE) {
        bool synced = mpithor::Reordering::get()->analyze();
        if (synced)
            mpithor::g_callstats.add_in_allreduce_analysis();
    }

// have to run Allreduce anyway, for data movement
// TODO: piggy-back on allreduce that to remove a collective call
    return MPI_Allreduce(send, recv, count, datatype, op, comm);
}

#ifdef MPICH

//
// replacement for MPI_Alltoall to switch on/off MPIC_xyz
//
static bool MPIC_ENABLED = false;
static bool MPIC_ALLTOALL = false;
int MPITHOR_DECL_WRAP(MPI_Alltoall)(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
                void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm)
{
    MPITHOR_TRACER(1);
    MPIC_ENABLED = MPIC_ALLTOALL = true;
    mpithor::Reordering::get()->new_region(MPIC_INTERNAL_MARKER);
    int result = MPI_Alltoall(sendbuf, sendcount, sendtype, recvbuf, recvcount, recvtype, comm);
    mpithor::Reordering::get()->new_region(DEFAULT_REGION_MARKER);
    MPIC_ENABLED = MPIC_ALLTOALL = false;
    return result;
}

//
// mpich internal Isend, Irecv, and Waitall as used in Alltoall.
//
int MPITHOR_DECL_WRAP(MPIC_Isend)(const void *buf, int count, MPI_Datatype datatype, int dest, int tag,
                void *comm_ptr, void **request, void *errflag)
{
    MPITHOR_TRACER(1);
    if (!MPIC_ENABLED || mpithor::g_state == mpithor::EO_DISABLED)
        return MPIC_Isend(buf, count, datatype, dest, tag, comm_ptr, request, errflag);
    mpithor::g_callstats.add_sendc();
    MessageQueue& queue = g_msg_queue[MPITHOR_ISENDC_MSG];
    queue.enqueue_request((void*)buf, count, datatype,
       dest, tag, (ptrdiff_t)comm_ptr, (MPI_Request*)request, errflag);
    if (g_issue_policy == EI_CONTINUOUS_ISSUE)
        Reordering::get()->issue_request_bundled(queue, fixed_count);
    return MPI_SUCCESS;
}

int MPITHOR_DECL_WRAP(MPIC_Irecv)(void *buf, int count, MPI_Datatype datatype, int source,
               int tag, void *comm_ptr, void **request)
{
    MPITHOR_TRACER(1);
    if (!MPIC_ENABLED || mpithor::g_state == mpithor::EO_DISABLED)
        return MPIC_Irecv(buf, count, datatype, source, tag, comm_ptr, request);
    mpithor::g_callstats.add_recvc();
    MessageQueue& queue = g_msg_queue[MPITHOR_IRECVC_MSG];
    queue.enqueue_request(buf, count, datatype, source, tag,
       (ptrdiff_t)comm_ptr, (MPI_Request*)request);
    if (g_issue_policy == EI_CONTINUOUS_ISSUE)
        Reordering::get()->issue_request_bundled(queue, fixed_count);
    return MPI_SUCCESS;
}

int MPITHOR_DECL_WRAP(MPIC_Sendrecv)(
        const void *sendbuf, MPI_Aint sendcount, MPI_Datatype sendtype, int dest, int sendtag,
        void *recvbuf, MPI_Aint recvcount, MPI_Datatype recvtype, int source, int recvtag,
        void *comm_ptr, MPI_Status *status, void *errflag)
{
// Reordering Sendrecv should only happen underneath Alltoall to make sure the
// behavior is as expected.
    MPITHOR_TRACER(1);
    if (dest == source || !MPIC_ALLTOALL || mpithor::g_state == mpithor::EO_DISABLED)
        return MPIC_Sendrecv(sendbuf, sendcount, sendtype, dest, sendtag,
                             recvbuf, recvcount, recvtype, source, recvtag,
                             comm_ptr, status, errflag);
    mpithor::g_callstats.add_sendrecvc();
    assert(MPITHOR_SENDRECVC_MSG < sizeof(g_msg_queue)/sizeof(g_msg_queue[0]));
    MessageQueue& queue = g_msg_queue[MPITHOR_SENDRECVC_MSG];
    queue.enqueue_request(
        sendbuf, sendcount, sendtype, dest, sendtag,
        recvbuf, recvcount, recvtype, source, recvtag,
        comm_ptr, status, errflag);

    if (g_issue_policy == EI_CONTINUOUS_ISSUE)
        Reordering::get()->issue_request_bundled(queue, fixed_count);

    return MPI_SUCCESS;
}

int MPITHOR_DECL_WRAP(MPIC_Waitall)(int numreq, void **requests, void *statuses, void *errflag)
{
// This function is internal to alltoall only, and therefore always matches
// the requests currently in the queue.
    MPITHOR_TRACER(1);
    if (!MPIC_ENABLED || mpithor::g_state == mpithor::EO_DISABLED)
        return MPIC_Waitall(numreq, requests, statuses, errflag);
    mpithor::g_callstats.add_waitallc();

    const std::shared_ptr<mpithor::Reordering>& reorder = mpithor::Reordering::get();

// issue remotes (these wait, so overlap in reverse is not possible)
    if (g_msg_queue[MPITHOR_SENDRECVC_MSG].ready_for_issue())
        reorder->issue_request_bundled(g_msg_queue[MPITHOR_SENDRECVC_MSG], all);

// Two options: either get here with our with use of mpithor API to define
// regions. If regions defined, then the region endings should have caused
// the issue. If not, then the outstanding should match the request.
    if (g_msg_queue[MPITHOR_IRECVC_MSG].ready_for_issue() ||
        g_msg_queue[MPITHOR_ISENDC_MSG].ready_for_issue()) {
        MPITHOR_TRACER_MSG(2, "draining queue");

    // TODO: the following only makes sense in the limited case of mpich's
    // alltoall implementation (which, of course, this is tailored on ...)
        assert(numreq/2 == (int)g_msg_queue[MPITHOR_IRECVC_MSG].ready_for_issue());
        assert(numreq/2 == (int)g_msg_queue[MPITHOR_ISENDC_MSG].ready_for_issue());

    // post all irecvcs
        reorder->issue_request_bundled(g_msg_queue[MPITHOR_IRECVC_MSG], all);

    // post all isendcs
        reorder->issue_request_bundled(g_msg_queue[MPITHOR_ISENDC_MSG], all);
    } else {
        MPITHOR_TRACER_MSG(2, "queue empty");
    }

// issue remotes (these wait, so overlap in reverse is not possible)
//    if (g_msg_queue[MPITHOR_SENDRECVC_MSG].ready_for_issue())
//        reorder->issue_request_bundled(g_msg_queue[MPITHOR_SENDRECVC_MSG], all);

// actual wait (both recv and send)
// TODO: pick out the send requests and wait on them first?
    int result = MPIC_Waitall(numreq, requests, statuses, errflag);
    if (result != MPI_SUCCESS)
        return result;

// finish measurements/analysis and its analysis (not needed for receives)
    if (g_state != EO_ANALYSIS_OFF)
        result = reorder->communication_done(MPITHOR_ISENDC_MSG);
    if (g_state == EO_ANALYSIS_PENDING)
        g_state = EO_ANALYSIS_OFF;

    // complete queue (TODO: tie this to the actual messages)
    std::vector<mpithor::MessageQueue::size_type> bundle;
    mpithor::MessageQueue& rq = g_msg_queue[MPITHOR_IRECVC_MSG];
    rq.get_bundle_for_completion(-1, bundle);
    for (auto ib : bundle) {
        assert(rq.get_request(ib).state == r_issued);
        rq.get_request(ib).state = r_completed;
    }
    bundle.clear();
    mpithor::MessageQueue& sq = g_msg_queue[MPITHOR_ISENDC_MSG];
    sq.get_bundle_for_completion(-1, bundle);
    for (auto ib : bundle) {
        assert(sq.get_request(ib).state == r_issued);
        sq.get_request(ib).state = r_completed;
    }

    return result;
}

#endif // MPICH
