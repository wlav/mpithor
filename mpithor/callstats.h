#ifndef __MPITHOR_CALLSTATS_H__
#define __MPITHOR_CALLSTATS_H__

#include <stddef.h>
#include <stdint.h>

#define MPITHOR_COUNTER_FUNC(what)                                            \
    void add_##what()    { num_##what    +=1; }                               \
    void add_##what##c() { num_##what##c +=1; }

#define MPITHOR_COUNTER(what)                                                 \
    uint64_t num_##what;                                                      \
    uint64_t num_##what##c;

#define MPITHOR_INITIALIZER(what)                                             \
    num_##what(0), num_##what##c(0)


namespace mpithor {

class CallStats {
public:
    CallStats() : MPITHOR_INITIALIZER(recv), MPITHOR_INITIALIZER(send),
                  num_sendrecvc(0),
                  MPITHOR_INITIALIZER(wait), MPITHOR_INITIALIZER(waitall),
                  num_barrier(0), num_bar_analyses(0),
                  num_allreduce(0), num_allred_analyses(0),
                  num_bundle_issued(0), tot_bundle_size(0.),
                  num_analyses(0), num_global(0), num_local(0),
                  num_won(0), num_lost(0), analysis_time(0.)
    { }

    void print_stats(int node);

// mpi-level stats
    MPITHOR_COUNTER_FUNC(recv)
    MPITHOR_COUNTER_FUNC(send)
    void add_sendrecvc() { num_sendrecvc += 1; }
    MPITHOR_COUNTER_FUNC(wait)
    MPITHOR_COUNTER_FUNC(waitall)
    void add_barrier() { num_barrier += 1; }
    void add_in_barrier_analysis() { num_bar_analyses += 1; }
    void add_allreduce() { num_allreduce += 1; }
    void add_in_allreduce_analysis() { num_allred_analyses += 1; }

// mpithor-level stats
    void add_bundle_issued(size_t sz) {
        num_bundle_issued += 1; tot_bundle_size += sz;
    }
    void add_analysis(double t) { num_analyses += 1; analysis_time += t; }
    void add_global()   { num_global   += 1; }
    void add_local()    { num_local    += 1; }
    void add_won()      { num_won      += 1; }
    void add_lost()     { num_lost     += 1; }

private:
    MPITHOR_COUNTER(recv)
    MPITHOR_COUNTER(send)
    uint64_t num_sendrecvc;
    MPITHOR_COUNTER(wait)
    MPITHOR_COUNTER(waitall)
    uint64_t num_barrier, num_bar_analyses;
    uint64_t num_allreduce, num_allred_analyses;

    uint64_t num_bundle_issued;
    double   tot_bundle_size;
    uint64_t num_analyses, num_global, num_local, num_won, num_lost;
    double   analysis_time;
};

extern CallStats g_callstats;

} // namespace mpithor

#endif // ! __MPITHOR_CALLSTATS_H__
