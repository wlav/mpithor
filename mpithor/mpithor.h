#ifndef __MPITHOR_MPITHOR_H__
#define __MPITHOR_MPITHOR_H__

#include "msgtypes.h"

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus

// public API with a few helper functions, usable from client code
    int mpithor_initialize();

    void mpithor_start_pause();           // temporarily disable reordering
    void mpithor_end_pause();             // end temporary diabling

    void mpithor_start_region(int msgtype, int marker);
    void mpithor_end_region(int msgtype, int marker);

    int mpithor_finalize();

    void mpithor_set_immediate_analysis(); // analysis "in-place"
    void mpithor_set_deferred_analysis();  // analysis in barriers

    void mpithor_set_keyed_analysis();     // regions identified by keys
    void mpithor_set_on_keyed_analysis();  // single analysis (no regions)

#ifdef __cplusplus
}
#endif // ifdef __cplusplus

#endif // ! __MPITHOR_MPITHOR_H__
