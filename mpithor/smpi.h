#ifndef __MPITHOR_SMPI_H__
#define __MPITHOR_SMPI_H__

#if !defined(MPITHOR_INTERNAL) && !defined(DISABLE_MPITHOR)
#warning MPITHOR is redefining mpi APIs

#if !defined(MPITHOR_LINK_OVERRIDES)
#define MPITHOR_DECL_WRAP(name) s##name
#else
#include <mpi.h>
#define MPITHOR_DECL_WRAP(name) name
#endif

#ifdef MPI_Init
#undef MPI_Init
#endif
#define MPI_Init                  MPITHOR_DECL_WRAP(MPI_Init)

#ifdef MPI_Init_thread
#undef MPI_Init_thread
#endif
#define MPI_Init_thread           MPITHOR_DECL_WRAP(MPI_Init_thread)

#ifdef MPI_Finalize
#undef MPI_Finalize
#endif
#define MPI_Finalize              MPITHOR_DECL_WRAP(MPI_Finalize)

#ifdef MPI_Isend
#undef MPI_Isend
#endif
#define MPI_Isend                 MPITHOR_DECL_WRAP(MPI_Isend)

#ifdef MPI_Irecv
#undef MPI_Irecv
#endif
#define MPI_Irecv                 MPITHOR_DECL_WRAP(MPI_Irecv)

#ifdef MPI_Wait
#undef MPI_Wait
#endif
#define MPI_Wait                  MPITHOR_DECL_WRAP(MPI_Wait)

#ifdef MPI_Waitall
#undef MPI_Waitall
#endif
#define MPI_Waitall               MPITHOR_DECL_WRAP(MPI_Waitall)

#ifdef MPI_Barrier
#undef MPI_Barrier
#endif
#define MPI_Barrier               MPITHOR_DECL_WRAP(MPI_Barrier)

#ifdef MPI_Allreduce
#undef MPI_Allreduce
#endif
#define MPI_Allreduce             MPITHOR_DECL_WRAP(MPI_Allreduce)

#ifdef MPICH
#ifdef MPI_Alltoall
#undef MPI_Alltoall
#endif
#define MPI_Alltoall              MPITHOR_DECL_WRAP(MPI_Alltoall)
#endif // MPICH

#else  // internal or disabled

#if !defined(MPITHOR_LINK_OVERRIDES)
#define MPITHOR_DECL_WRAP(name) s##name
#else
#define MPITHOR_DECL_WRAP(name) __wrap_##name
#endif

#include <mpi.h>

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus

    int MPITHOR_DECL_WRAP(MPI_Init)(int *argc, char ***argv);
    int MPITHOR_DECL_WRAP(MPI_Init_thread)(int *argc, char ***argv, int required, int *provided);
    int MPITHOR_DECL_WRAP(MPI_Isend)(const void *buf, int count, MPI_Datatype datatype, int dest,
                   int tag, MPI_Comm comm, MPI_Request *request);
    int MPITHOR_DECL_WRAP(MPI_Irecv)(void *buf, int count, MPI_Datatype datatype, int source,
                   int tag, MPI_Comm comm, MPI_Request *request);
    int MPITHOR_DECL_WRAP(MPI_Wait)(MPI_Request *request, MPI_Status *status);
    int MPITHOR_DECL_WRAP(MPI_Waitall)(int count, MPI_Request array_of_requests[],
                     MPI_Status array_of_statuses[]);	
    int MPITHOR_DECL_WRAP(MPI_Barrier)(MPI_Comm comm);
    int MPITHOR_DECL_WRAP(MPI_Allreduce)(void* send, void* recv, int count, MPI_Datatype datatype,
                       MPI_Op op, MPI_Comm comm);
    int MPITHOR_DECL_WRAP(MPI_Finalize)();

#ifdef MPICH
    int MPITHOR_DECL_WRAP(MPI_Alltoall)(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
               void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm);
    
// mpich internal functions, allowing replacement of alltoall innards.
// Note the use of void*'s instead of internal data structs, to remove
// the need of defining them here
    int MPITHOR_DECL_WRAP(MPIC_Isend)(const void *buf, int count, MPI_Datatype datatype,
               int dest, int tag, void *comm_ptr, void **request, void *errflag);
    int MPITHOR_DECL_WRAP(MPIC_Irecv)(void *buf, int count, MPI_Datatype datatype, int source,
               int tag, void *comm_ptr, void **request);
    int MPITHOR_DECL_WRAP(MPIC_Sendrecv)(
        const void *sendbuf, MPI_Aint sendcount, MPI_Datatype sendtype, int dest, int sendtag,
        void *recvbuf, MPI_Aint recvcount, MPI_Datatype recvtype, int source, int recvtag,
        void *comm_ptr, MPI_Status *status, void *errflag);
    int MPITHOR_DECL_WRAP(MPIC_Waitall)(int numreq, void **requests, void *statuses, void *errflag);
#endif // MPICH

#ifdef MPITHOR_LINK_OVERRIDES

#ifdef MPI_Init
#undef MPI_Init
#endif
#define MPI_Init                  __real_MPI_Init

#ifdef MPI_Init_thread
#undef MPI_Init_thread
#endif
#define MPI_Init_thread           __real_MPI_Init_thread

#ifdef MPI_Finalize
#undef MPI_Finalize
#endif
#define MPI_Finalize              __real_MPI_Finalize

#ifdef MPI_Isend
#undef MPI_Isend
#endif
#define MPI_Isend                 __real_MPI_Isend

#ifdef MPI_Irecv
#undef MPI_Irecv
#endif
#define MPI_Irecv                 __real_MPI_Irecv

#ifdef MPI_Wait
#undef MPI_Wait
#endif
#define MPI_Wait                  __real_MPI_Wait

#ifdef MPI_Waitall
#undef MPI_Waitall
#endif
#define MPI_Waitall               __real_MPI_Waitall

#ifdef MPI_Barrier
#undef MPI_Barrier
#endif
#define MPI_Barrier               __real_MPI_Barrier

#ifdef MPI_Allreduce
#undef MPI_Allreduce
#endif
#define MPI_Allreduce             __real_MPI_Allreduce

#ifdef MPICH

#ifdef MPI_Alltoall
#undef MPI_Alltoall
#endif
#define MPI_Alltoall              __real_MPI_Alltoall

#ifdef MPIC_Isend
#undef MPIC_Isend
#endif
#define MPIC_Isend                __real_MPIC_Isend

#ifdef MPIC_Irecv
#undef MPIC_Irecv
#endif
#define MPIC_Irecv                __real_MPIC_Irecv

#ifdef MPIC_Sendrecv
#undef MPIC_Sendrecv
#endif
#define MPIC_Sendrecv             __real_MPIC_Sendrecv

#ifdef MPIC_Wait
#undef MPIC_Wait
#endif
#define MPIC_Wait                 __real_MPIC_Wait

#ifdef MPIC_Waitall
#undef MPIC_Waitall
#endif
#define MPIC_Waitall              __real_MPIC_Waitall

#endif // MPICH

#endif // MPITHOR_LINK_OVERRIDES

    int MPI_Init(int *argc, char ***argv);
    int MPI_Init_thread(int *argc, char ***argv, int required, int *provided);
    int MPI_Finalize();
    int MPI_Isend(const void *buf, int count, MPI_Datatype datatype, int dest, int tag,
               MPI_Comm comm, MPI_Request *request);
    int MPI_Irecv(void *buf, int count, MPI_Datatype datatype, int source,
               int tag, MPI_Comm comm, MPI_Request *request);
    int MPI_Wait(MPI_Request *request, MPI_Status *status);
    int MPI_Waitall(int count, MPI_Request array_of_requests[], 
               MPI_Status array_of_statuses[]);
    int MPI_Barrier(MPI_Comm comm);
    int MPI_Allreduce(const void *sendbuf, void *recvbuf, int count,
               MPI_Datatype datatype, MPI_Op op, MPI_Comm comm);

#ifdef MPICH
// public
    int MPI_Alltoall(const void *sendbuf, int sendcount, MPI_Datatype sendtype,
               void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm);

// internal
    int MPIC_Isend(const void*, int, MPI_Datatype, int, int, void*, void**, void*);
    int MPIC_Irecv(void*, int, MPI_Datatype, int, int, void*, void**);
    int MPIC_Sendrecv(const void *sendbuf, MPI_Aint sendcount, MPI_Datatype sendtype,
                      int dest, int sendtag, void *recvbuf, MPI_Aint recvcount,
                      MPI_Datatype recvtype, int source, int recvtag,
                      void *comm_ptr, MPI_Status *status, void *errflag);
    int MPIC_Waitall(int numreq, void **requests, void *statuses, void *errflag);
#endif // MPICH

#ifdef __cplusplus
}
#endif // ifdef __cplusplus

#endif // !MPITHOR_INTERNAL

#endif // !__MPITHOR_SMPI_H__
