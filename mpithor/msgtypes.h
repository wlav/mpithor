#ifndef __MPITHOR_MSGTYPES_H__
#define __MPITHOR_MSGTYPES_H__

// available request types
#define MPITHOR_ISEND_MSG     0
#define MPITHOR_IRECV_MSG     1
#define MPITHOR_ISENDC_MSG    2
#define MPITHOR_IRECVC_MSG    3
#define MPITHOR_SENDRECVC_MSG 4

#endif // ! __MPITHOR_MSGTYPES_H__
