#ifndef __MPITHOR_REORDERING_H__
#define __MPITHOR_REORDERING_H__

#include <algorithm>
#include <chrono>
#include <map>
#include <memory>
#include <utility>
#include <vector>

#include <float.h>

namespace mpithor {

// individual message posting reordering strategies

class Reordering {
public:
    virtual ~Reordering();

    virtual int initialize() { return MPI_SUCCESS; }
    virtual int new_region(int) { return MPI_SUCCESS; }
    virtual int reset() { return MPI_SUCCESS; }

    virtual bool analyze() { return false; }

    virtual size_t issue_request_bundled(
        MessageQueue& queue, issue_policy_t policy) = 0;

    virtual size_t issue_bundle(
        MessageQueue& queue, const std::vector<MessageQueue::size_type>& bundle);
    virtual int communication_done(int message_type) { return MPI_SUCCESS; }

public:
    static std::shared_ptr<Reordering> get() {
        static std::shared_ptr<Reordering> reordering = get_reordering();
        return reordering;
    }

    typedef std::vector<MessageQueue::size_type> Bundle_t;

protected:
    Bundle_t m_bundle;

private:
    static std::unique_ptr<Reordering> get_reordering();
};


//
// Simply post messages immediately, with no bundling or reordering
//
class Immediate : public Reordering {
public:
    virtual int initialize();

    virtual size_t issue_request_bundled(
            MessageQueue& queue, issue_policy_t /* policy */) {
        MPITHOR_TRACER(2);

        if (!queue.ready_for_issue())
            return 0;

        queue.get_bundle_for_issue(-1 /* all */, m_bundle);
        return issue_bundle(queue, m_bundle);
    }
};

//
// Helper base class for HighLow and LowHigh
//
class SimpleOrdered : public Reordering {
public:
    SimpleOrdered(int bundle_size) : m_bundle_size(bundle_size) {
        m_bundle.resize(m_bundle_size);
    }

public:
    virtual int initialize();

    virtual size_t issue_request_bundled(
        MessageQueue& queue, issue_policy_t policy);

    virtual void sort_bundle(MessageQueue& queue, Reordering::Bundle_t& bundle,
        latency_t* latencies, latency_t* injection) = 0;

protected:
    int m_bundle_size;
};


//
// Post messages with high to low latency ordering, with assigned bundling
//
class HighLow : public SimpleOrdered {
public:
    using SimpleOrdered::SimpleOrdered;
    virtual void sort_bundle(MessageQueue& queue, Reordering::Bundle_t& bundle,
        latency_t* latencies, latency_t* injection);
};


//
// Post messages with low to high latency ordering, with assigned bundling.
//
class LowHigh : public SimpleOrdered {
public:
    using SimpleOrdered::SimpleOrdered;
    virtual void sort_bundle(MessageQueue& queue, Reordering::Bundle_t& bundle,
        latency_t* latencies, latency_t* injection);
};


//
// Post message in an ordering according to an estimator
//
class LocalSchedule : public SimpleOrdered {
    using SimpleOrdered::SimpleOrdered;
    virtual void sort_bundle(MessageQueue& queue, Reordering::Bundle_t& bundle,
        latency_t* latencies, latency_t* injection);
};


//
// Helper class containing all the analysis stats, allowing a per (size, #neighbors)
// grouping of rounds for the global schedule based reordering
//
class AnalysisData {
public:
    AnalysisData(size_t warmup, size_t commsz,
        const Reordering::Bundle_t& bundle, MessageQueue& queue) :
            m_max_rounds(0), m_skip(warmup), m_best_average(FLT_MAX),
            m_slowest_node((size_t)-1), m_state(EA_GLOBAL) {

        m_comm_times.reserve(commsz);

        m_order_mapping.resize(bundle.size());
        size_t iorder = 0;
        for (auto ib : bundle) {
            mem_req_t& mr = queue.get_request(ib);
            m_order_mapping[iorder] = std::make_pair(iorder, mr.target);
            iorder += 1;
        }

        m_is_active = false;
    }

public:
// max number of communication rounds (for broadcast)
    size_t m_max_rounds;

// current order
    std::vector<std::pair<size_t, int>> m_order_mapping;

// saved order in case of reversal (could recalculate, but this is
// easier than dealing with nodes that have fewer than max neighbors)
    std::vector<std::pair<size_t, int>> m_last_order_mapping;

// measurements
    typedef std::chrono::steady_clock clock_t;
    typedef std::vector<float>::size_type count_t;

    std::vector<float> m_comm_times;
    clock_t::time_point m_clock_start;
    count_t m_skip;

// history
    std::vector<std::vector<size_t>> m_orderings;
    std::vector<float> m_sd_times;
    std::vector<int> m_order;
    float m_best_average;
    size_t m_slowest_node;

// analysis driver (separate b/c all nodes, to synchronize barriers)
    enum AnalysisState { EA_GLOBAL=1, EA_LOCAL=2, EA_REMEASURE=3, EA_STOPPED=4 };
    AnalysisState m_state;

    bool m_is_active;
};


// individual message posting reordering strategies

//
// Instrument communication phase of fixed domain decomposition and reorder
// based on a global consensus
//
class GlobalSchedule : public Reordering {
    typedef AnalysisData::count_t count_t;
public:
    GlobalSchedule(int interval, int warmup, int remeasure);
    GlobalSchedule(const GlobalSchedule&) = delete;
    GlobalSchedule& operator=(const GlobalSchedule&) = delete;

public:
    virtual int initialize();
    virtual int new_region(int marker);
    virtual int reset();

    virtual bool analyze();

    virtual size_t issue_request_bundled(
            MessageQueue& queue, issue_policy_t policy);
    virtual int communication_done(int message_type);

private:
// configuration
    const count_t m_interval, m_warmup, m_remeasure;

// analysis is done per communication region (marker) and per
// bundle, identified by hash(sizes), hash(ranks) pairs
    typedef std::pair<uint64_t, uint64_t> AnalysisKey_t;
    std::map<int, std::map<AnalysisKey_t, AnalysisData>> m_all_analyses;

// cache of pointers to prevent needless lookups
    int m_last_marker;
    std::map<AnalysisKey_t, AnalysisData>* m_cur_analyses;
    AnalysisData* m_cur_analysis_data;

// keep track on data collection whether an analysis should be run at all
    bool m_data_ready;

// helpers
    AnalysisKey_t get_key(MessageQueue& queue, const Reordering::Bundle_t&);
    void reorder(Reordering::Bundle_t&, const AnalysisData&);
};

} // namespace mpithor 

#endif // ! __MPITHOR_REORDERING_H__
