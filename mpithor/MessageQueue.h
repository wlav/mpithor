#ifndef __MPITHOR_MessageQUEUE_H__
#define __MPITHOR_MessageQUEUE_H__

#include <mpi.h>
#include "datatypes.h"
#include "config.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <vector>
#include <assert.h>
#include <stddef.h>
#include <map>


namespace mpithor {

// queue of filed requests; for now, we use one queue per MPI rank and
// per type of request
class MessageQueue {
public:
    typedef std::array<mem_req_t, REQ_QUEUE_LENGTH>::size_type size_type;   // define size_type

public:
    MessageQueue(int msgtype) : m_msgtype(msgtype) {
    // m_head points to the one before the first unused slot (%length), m_tail_issued
    // to the last slot that was issued (%length; state == r_issued) and m_tail_completed
    // to the last completed request (%length) and is again potentially available
    //
    //  | ...              |
    //  | r_completed      |
    //  | ...              |
    //  | r_completed      |  <- m_tail_completed
    //  | r_issued         |
    //  | ...              |
    //  | r_issued         |  <- m_tail_issued
    //  | r_pending        |
    //  | ...              |
    //  | r_pending        |  <- m_head
    //  | 0 or r_completed |
    //
    // Numbers are only incremented: to get the slot, take %length. The
    // invariant that must hold is:
    //  m_tail_completed <= m_tail_issued <= m_head
    //
    // Note: the first slot will be skipped the first time around
        m_head = m_tail_issued = m_tail_completed = 0;
        m_requests[0].state = r_completed;
    }

public:
    int msgtype() { return m_msgtype; }

public:
    void* enqueue_request(void *buf, int count, MPI_Datatype datatype, int dest,
        int tag, ptrdiff_t comm, void *request, void *errflag = nullptr);
#ifdef MPICH
    void* enqueue_request(
        const void *sendbuf, MPI_Aint sendcount, MPI_Datatype sendtype, int dest, int sendtag,
        void *recvbuf, MPI_Aint recvcount, MPI_Datatype recvtype, int source, int recvtag,
        void *comm_ptr, MPI_Status *status, void *errflag);
#endif

    mem_req_t& get_request(size_type index) {
        return m_requests[index];
    }

// Get a bundle of indices of messages ready to be issued; after this call,
// the messages are assumed to be unavailable for other servers.
    size_type ready_for_issue() {
        return m_head - m_tail_issued;
    }

    bool ready_for_issue(int bundle_size) {
        if (0 < bundle_size && (m_head - m_tail_issued < (size_type)bundle_size))
            return false;
        return ready_for_issue();
    }

    void get_bundle_for_issue(int bundle_size, std::vector<size_type>& bundle) {
        assert(ready_for_issue(bundle_size));     // insist on external check
        if (bundle_size < 0) {
            assert(m_tail_issued <= m_head);
            bundle_size = m_head - m_tail_issued; // i.e. all outstanding
        }
        assert(0 < bundle_size);
        bundle.resize(bundle_size);

        int rel_start = m_tail_issued + 1;
        m_tail_issued += bundle_size;
        assert(m_tail_completed <= m_tail_issued && m_tail_issued <= m_head);

        for (int ib = 0; ib < bundle_size; ++ib)
            bundle[ib] = (rel_start + ib) % REQ_QUEUE_LENGTH;

    // the queue assumes that the bundle is now issued, but the requests are
    // not marked as such; if the queue is not large enough, things will roll
    // over earlier (and should be handled in enqueue_request)
    }

// Get a bundle of indices of messages that have either been handed out for issue
// (and may be still pending) or are issued and ready to be waited on; after this
// call, the messages are assumed to be unavailable for other servers.
    void get_bundle_for_completion(int bundle_size, std::vector<size_type>& bundle) {
        if (0 < bundle_size) {
            bundle_size = std::min(m_head - m_tail_completed, (size_type)bundle_size);
        } else {
            bundle_size = m_head - m_tail_completed;
            bundle.resize(bundle_size);
        }

        int rel_start = m_tail_completed + 1;
        m_tail_completed += bundle_size;
        assert(m_tail_completed <= m_tail_issued && m_tail_issued <= m_head);

        for (int ib = 0; ib < bundle_size; ++ib)
            bundle[ib] = (rel_start + ib) % REQ_QUEUE_LENGTH;

    // the queue assumes that the bundle is now completed, but the requests are
    // not marked as such; if the queue is not large enough, things will roll
    // over earlier (and should be handled in enqueue_request)
    }

private:
    std::array<mem_req_t, REQ_QUEUE_LENGTH> m_requests;   // queue of outstanding requests

// TODO: currently no padding to prevent false sharing, as we don't
// support threads yet
    volatile size_type m_head;                    // one before available slot (%length)

    volatile size_type m_tail_issued;             // last issued request (%length)
    volatile size_type m_tail_completed;          // last completed request (%length)

    const int m_msgtype;
};

} // namespace mpithor

#endif // ! __MPITHOR_MessageQUEUE_H__
