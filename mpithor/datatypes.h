#ifndef __MPITHOR_DATATYPES_H__
#define __MPITHOR_DATATYPES_H__

#include <stddef.h>

namespace mpithor {

// possible request states
typedef enum {r_completed=123, r_pending=456, r_issued=789} req_state_t;

// issue policy for draining queue
typedef enum {all, only_aged, fixed_count, credit, timed} issue_policy_t;

// one individual request
typedef struct mem_req_struct {
    void* 	  buf;
    size_t        count;
    int           target;          // destination for send, source for receive
    MPI_Datatype  datatype;
    int           tag;
    ptrdiff_t     comm;
    void*         request;
    void*         errflag;

    volatile req_state_t state;    // processing state of request

#ifdef MPICH
// second set for MPIC_Sendrecv
    void* 	  tbuf;
    size_t        tcount;
    int           tsource;
    MPI_Datatype  tdatatype;
    int           ttag;
#endif

#if defined(CEREBRO_) || defined(CORI_)
#ifndef MPICH
    // no padding: data struture is 64b
#else
    char padding[32];              // rounds to 129b
#endif
#elif defined SHEPARD_
    char padding[56];              // rounds to 128b
#else
#ifndef MPITHOR_PADDING_VERIFY
#error "VERIFY PADDING!"
   //char padding[N];
#endif
#endif
} mem_req_t;

typedef double latency_t;

} // namespace mpithor

#endif // ! __MPITHOR_DATATYPES_H__
