#ifndef __MPITHOR_INTERNAL_H__
#define __MPITHOR_INTERNAL_H__

#include "msgtypes.h"

namespace mpithor {
    enum EState { EO_ANALYSIS_OFF, EO_ANALYSIS_ON, EO_ANALYSIS_PENDING, EO_DISABLED };
    extern EState g_state;

#define DEFAULT_REGION_MARKER -71
#define MPIC_INTERNAL_MARKER -103
#ifndef NDEBUG
// current communication region; only used for asserting start/end matches
    extern int g_current_region;
#endif

    extern bool MPITHOR_REORDER_RECVS;
    extern bool MPITHOR_ANALYSIS_IN_BARRIER;
    extern bool MPITHOR_ANALYSIS_IN_ALLREDUCE;
    extern bool MPITHOR_KEYED_ANALYSIS;

    extern int MPITHOR_ANALYSIS_RANK;

// policy (to be set by reordering) for calling to issue after enqueueing
    enum EIssuePolicy { EI_CONTINUOUS_ISSUE, EI_DRAIN_ONLY };
    extern EIssuePolicy g_issue_policy;

// save some mpi info
    extern int g_world_rank;
    extern int g_world_size;

    inline bool is_recv_type(int msgtype) {
        return msgtype == MPITHOR_IRECV_MSG || msgtype == MPITHOR_IRECVC_MSG;
    }

    inline bool is_send_type(int msgtype) {
        return msgtype == MPITHOR_ISEND_MSG || msgtype == MPITHOR_ISENDC_MSG || msgtype == MPITHOR_SENDRECVC_MSG;
    }

} // namespace mpithor

#endif // ! __MPITHOR_INTERNAL_H__
