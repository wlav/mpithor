#ifndef __MPITHOR_MESSAGE_TIMINGS_H__
#define __MPITHOR_MESSAGE_TIMINGS_H__

#include <sys/types.h>

namespace mpithor {

// To be called after setting up MPI. The concurrency parameter determines
// how many messages are send at the same time to collect timings: this number
// should be representative of the application (i.e. the typical number of
// off-node communications in the typical communication round).
int collect_timings(int concurrency);

// Unlike gnthor, data in mpithor is only collected by size, not by target.
// There are, however, more variations possible b/c of having both recvs
// and sends. To keep the number of combinations manageable, we assume that
// recvs just need to follow the same or reversed order as the sends.
latency_t send_latency(size_t blocksize);
latency_t send_injection(size_t blocksize);

} // namespace mpithor

#endif // ! __MPITHOR_MESSAGE_TIMINGS_H__
