#ifndef __MPITHOR_DEBUG_H__
#define __MPITHOR_DEBUG_H__

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif // ifdef __cplusplus
    int GET_MYRANK();
#ifdef __cplusplus
}
#endif // ifdef __cplusplus

extern int MPITHOR_VERBOSE;
extern int MPITHOR_VERBOSE_RANK;

#ifndef MPITHOR_NO_TRACING
#include <iostream>
#define MPITHOR_TRACER_MSG(level, msg) do { if ((level <= MPITHOR_VERBOSE) && (MPITHOR_VERBOSE_RANK == -1 || MPITHOR_VERBOSE_RANK == GET_MYRANK())) { std::cerr << "[" << GET_MYRANK() << "] " << __FUNCTION__ << ": " << __FILE__ << " " << __LINE__ << " " << msg << std::endl; } } while(0);
#define MPITHOR_TRACER(level) MPITHOR_TRACER_MSG(level, "")
#else
#define MPITHOR_TRACER_MSG(level, msg)
#define MPITHOR_TRACER(level)
#endif

#endif // !__MPITHOR_DEBUG_H__
