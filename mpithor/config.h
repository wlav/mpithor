#ifndef __MPITHOR_CONFIG_H__
#define __MPITHOR_CONFIG_H__

// TODO: set this to 65535, the maximum number of explicit gasnet handles in flight
// number is currently low just for debugging
#define REQ_QUEUE_LENGTH 65535

// fixed size block of maximum bundle reordering (dynamic is typically 8)
#define MAX_STATIC_REORDER_BUNDLE_SIZE 32

#endif // ! __MPITHOR_CONFIG_H__
