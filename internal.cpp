#include "mpithor/config.h"

#define MPITHOR_INTERNAL 1
#include "mpithor/internal.h"

mpithor::EState mpithor::g_state = mpithor::EO_ANALYSIS_ON;
#ifndef NDEBUG
int mpithor::g_current_region = DEFAULT_REGION_MARKER;
#endif
mpithor::EIssuePolicy mpithor::g_issue_policy = mpithor::EI_DRAIN_ONLY;
